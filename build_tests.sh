#!/bin/bash
# script builds the emulator/sender/receiver docker container either with a tag referring to the current
# branch name or with a release tag depending whether this is a development or deployment
# version.
if [[ "${BASH_SOURCE[0]}" != "${0}" ]]
then
  echo "script ${BASH_SOURCE[0]} is being sourced please execute it! \n. ./${BASH_SOURCE[0]} local"
  return 1
fi

case "$1" in
    "local")
        echo "Building test version of emulator package including everything ..."
        #docker build --build-arg --no-cache -t ska-sdp-cbf-emulator:local -f docker/Dockerfile-tests .
        docker build -t ska-sdp-cbf-emulator:local -f docker/Dockerfile-tests .
        docker build -t ska-sdp-cbf-emulator:recv -f docker/Dockerfile-recv .
        docker build -t ska-sdp-cbf-emulator:send -f docker/Dockerfile-send .
        docker build -t casacore-tools:local -f docker/Dockerfile-taql .
        echo "Build finished!"
        exit 0;;
    *)
        echo "Usage: build_engine.sh <local>"
        exit 1;;
esac

