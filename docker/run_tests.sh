DOCKER_OPTS="\
--rm " 

EMU_ROOT="/tmp/emu"
case "$1" in
    "send")
        echo "Starting sender"
        if [ -z "$2" ]; then
            echo "No configuration supplied usage: ./run_tests.sh send <config-file> <file-to-send>"
            exit 1
        fi
        if [ -z "$3" ]; then
            echo "No file-to-send supplied usage: ./run_tests.sh send <config-file> <file-to-send>"
            exit 1
        fi


        DOCKER_OPTS=${DOCKER_OPTS}" --name sender -v ${EMU_ROOT}:${EMU_ROOT} "
        docker run -itd ${DOCKER_OPTS}  ska-sdp-cbf-emulator:send emu-send -c "$2" "$3"      
        return 0;;
    "recv")
        echo "Running Receiver in the background"
        if [ -z "$2" ]; then
            echo "No configuration supplied usage: ./run_tests.sh recv <config-file>"
            exit 1         
        fi
        DOCKER_OPTS=${DOCKER_OPTS}" --name receiver -v ${EMU_ROOT}:${EMU_ROOT} -p 41000-42000:41000-42000/udp "
        docker run -itd ${DOCKER_OPTS}  ska-sdp-cbf-emulator:recv emu-recv -c "$2" 
        return 0;;
     "plasma")
        echo "Running the Plasma Store in the background"
        DOCKER_OPTS=${DOCKER_OPTS}" --name plasma "
        docker run -td ${DOCKER_OPTS}  ska-sdp-cbf-emulator:plasma  plasma_store -s /mnt/socket/plasma -m 10000000
        return 0;;
     "plasma-mswriter")
        echo "Running the plasma client in the background"
        DOCKER_OPTS=${DOCKER_OPTS}" --name plasma-mswriter "
        docker run -td ${DOCKER_OPTS}  ska-sdp-cbf-emulator:plasma  plasma-mswriter -s /mnt/socket/plasma ${EMU_ROOT}/plasma.ms
        return 0;;
    *)
        echo "Usage run_tests.sh <plasma|recv|send|plasma-mswriter>"
        return 1;;
esac
