# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

include .make/base.mk
include .make/python.mk
include .make/oci.mk

PYTHON_LINE_LENGTH = 99
DOCS_SPHINXOPTS = -W --keep-going
PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal
MEASUREMENT_SETS_FOR_TESTS = sim-vis.ms.tar.gz AA05LOW.ms.tar.gz

python-pre-test:
	./extract_data.sh $(MEASUREMENT_SETS_FOR_TESTS)
