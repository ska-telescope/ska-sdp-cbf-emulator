This repository now has a set of docker images that can be built and used.

These can be used to run a number of tests by hand without fully installing the package
```angular2html
cd ../
./docker/build_tests.sh local
cd tests/system/scripts

```
In the scripts directory you will see a number of bash scripts. These come in pairs

```angular2html
SP-XXXX-XXX-TEST.sh &
SP-XXXX-SEND.sh
```
You should run the "TEST" first as that usually sets up the environment. And the "SEND" stars sending the packets.
