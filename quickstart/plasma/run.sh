#!/bin/bash
# Really simple send and recv.
# Just launch the receiver - give it a second or two to start
# Then launch the sender
# Assuming you have a virtualenv installed in venv
# 
source ../../venv/bin/activate

# kill any existing procs
ps -e | grep emu | grep -v grep | awk '{print $1}' | xargs kill

#
# Start the plasma store
plasma_store -m 1000000000 -s /tmp/plasma &
# Need to start a PLASMA consumer that will connect to the store
# The DAL needs a registered consumer of the plasma store data
# This should try and write a measurement set
plasma-mswriter ./new-vis.ms &
# Start the SPEAD2 receiver - this also fills the store
sleep 1
emu-recv -c ./50000ch.conf &
# if you get a bind error you have a receiver still running somewhere
sleep 5
# Start sending the test data
# The visibility set is a simple point source simulation - but the layout is 4 LOW antennas
# There are 50000 channels in the sumulation
emu-send -c ./50000ch.conf ../../data/50000ch-model.ms

# clean-up
sleep 5
ps -e | grep emu | grep -v grep | awk '{print $1}' | xargs kill
