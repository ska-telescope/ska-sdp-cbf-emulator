#!/bin/bash
# Really simple send and recv.
# 1000 channels this time - you can still test this on your laptop
# Just launch the receiver - give it a second or two to start
# Then launch the sender
# Assuming you have a virtualenv installed in venv
# 
source ../../venv/bin/activate
#
# Start the receiver 
emu-recv -c ./50000ch.conf &
# if you get a bind error you have a receiver still running somewhere
sleep 5
# Start sending the test data
# The visibility set is a simple point source simulation - but the layout is 4 LOW antennas
# There are 50000 channels in the sumulation
emu-send -c ./50000ch.conf ../../tests/data/50000ch-vis.ms
# the received measurement set will be written to this directory and you should be able to 
# image it - using any tool that will image a CASA Measurement set
