"""Data Source abstractions"""

from abc import ABCMeta, abstractmethod
from contextlib import AbstractAsyncContextManager
from dataclasses import dataclass
from functools import cached_property
from typing import AsyncIterable

import numpy as np
from overrides import override
from realtime.receive.core import icd
from realtime.receive.core.channel_range import ChannelRange


@dataclass(frozen=True)
class CorrelatedDatum:
    """A single time step's worth of data"""

    unix_timestamp: float
    """The unix timestamp of this datum"""

    visibilities: np.ndarray
    """
    All visibilities for the given timestamp in MS order
    (num_baselines, num_channels, num_polarisations)
    """

    normalised_weights: np.ndarray
    """
    The fraction of visibilities that have been correlated, with
    0.0 <= values <= 1.0 and shape (num_baselines, num_channels)
    """

    def __post_init__(self):
        assert len(self.visibilities.shape) == 3
        assert len(self.normalised_weights.shape) == 2
        assert (
            self.visibilities.shape[0] == self.normalised_weights.shape[0]
        ), "Number of baselines not consistent between visibilities and weights"
        assert (
            self.visibilities.shape[1] == self.normalised_weights.shape[1]
        ), "Number of channels not consistent between visibilities and weights"
        assert np.all(self.normalised_weights >= 0.0) and np.all(
            self.normalised_weights <= 1.0
        ), "Normalised weights must be 0 <= value <= 1"

    @cached_property
    def icd_visibilities(self):
        """The visibilities, in ICD order (num_channels, num_baselines, num_pol)"""
        return icd.ms_to_icd(self.visibilities)

    @cached_property
    def icd_corr_data_frac(self):
        """
        The weights, in ICD order (num_channels, num_baselines),
        as a correlated data fraction
        """
        return np.swapaxes(self.normalised_weights * 255, 0, 1).astype("u1")


class CorrelatedDataSource(AbstractAsyncContextManager, metaclass=ABCMeta):
    """A source of emulated data that can be transmitted."""

    @override
    async def __aenter__(self):
        return self

    @override
    async def __aexit__(self, exc_type, exc_value, traceback):
        pass

    @property
    @abstractmethod
    def name(self):
        """The name of this data source"""
        raise NotImplementedError

    @property
    @abstractmethod
    def info(self) -> list[tuple[str, str]]:
        """
        Gets information about this source as key-value pairs for
        the purposes of logging.
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def num_baselines(self) -> int:
        """The number of baselines in the visibility data"""
        raise NotImplementedError

    @property
    @abstractmethod
    def channels(self) -> ChannelRange:
        """
        The channel ids that will be sent alongside visibility data.
        `ChannelRange.count` must correspond with the channel (second) dimension
        in `visibilities()`.
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def channel_frequencies(self) -> np.ndarray:
        """The frequencies of each channel in Hz"""
        raise NotImplementedError

    @property
    @abstractmethod
    def channel_resolution(self) -> float:
        """The resolution of each channel in Hz"""
        raise NotImplementedError

    @property
    @abstractmethod
    def integration_period(self) -> float:
        """The amount of time spent integrating the signal at each time point"""
        raise NotImplementedError

    @property
    @abstractmethod
    def visibility_epoch(self) -> float:
        """The unix timestamp of earliest visibility data"""
        raise NotImplementedError

    @abstractmethod
    async def data(self) -> AsyncIterable[CorrelatedDatum]:
        """
        Create an iterable object that will return datums for each timestamp
        in the data source.
        """
        raise NotImplementedError
