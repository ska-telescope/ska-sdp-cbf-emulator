"""Classes needed to generate hardcoded visibilities"""

import copy
import time
from dataclasses import dataclass, field, fields
from typing import AsyncIterable

import numpy as np
from overrides import override
from realtime.receive.core.baseline_utils import baselines
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.common import autocast_fields

from .correlated_data_source import CorrelatedDataSource, CorrelatedDatum


@dataclass
@autocast_fields
# pylint: disable-next=too-many-instance-attributes
class HardcodedDataSourceConfig:
    """
    Configuration options for :class:`HardcodedDataSource`
    """

    num_stations: int
    """The number of stations in the hardcoded visibilities"""

    channels: ChannelRange | str
    """
    The channel ids (in `start_id:count[:stride]` format if a `str`)
    that will be sent with the hardcoded visibilities
    """

    num_timesteps: int
    """The number of timesteps that should be generated"""

    include_autocorrelations: bool = True
    """Whether autocorrelations are also included"""

    visibility_epoch: float = field(default_factory=time.time)
    """The Unix timestamp of the first timestep of visibilities"""

    visibility_interval: float = 1.0
    """The gap (in seconds) between each timestep"""

    num_polarisations: int = 4
    """The number of polarisations in generated visibilities"""

    dtype: str = "<c8"
    """The numpy.dtype of generated visibilities"""

    first_channel_freq: float = 1.0
    """The frequency (in Hz) of the first channel transmitted"""

    channel_resolution: float = 1.0
    """The bandwidth of each channel (in Hz)"""

    integration_period: float = 0.9
    """The time (in sec) of each integration period"""

    @property
    def channel_range(self):
        """Gets `channels` as a ChannelRange"""
        return (
            self.channels
            if isinstance(self.channels, ChannelRange)
            else ChannelRange.from_str(self.channels)
        )


class HardcodedDataSource(CorrelatedDataSource):
    """Generates visibility data according to hardcoded parameters."""

    def __init__(self, config: HardcodedDataSourceConfig):
        super().__init__()
        # TODO(yan-1550): Doesn't need to be a copy when we have a frozen dataclass
        self._config = copy.copy(config)

    @property
    @override
    def name(self):
        return "Hardcoded"

    @property
    @override
    def info(self) -> list[tuple[str, str]]:
        return [(f.name, getattr(self._config, f.name)) for f in fields(self._config)]

    @property
    @override
    def num_baselines(self) -> int:
        return baselines(self._config.num_stations, self._config.include_autocorrelations)

    @property
    @override
    def channels(self) -> ChannelRange:
        return self._config.channel_range

    @property
    @override
    def channel_frequencies(self) -> np.ndarray:
        return np.linspace(
            self._config.first_channel_freq,
            self._config.first_channel_freq
            + self._config.channel_resolution * self.channels.count,
            self.channels.count,
            endpoint=False,
        )

    @property
    @override
    def channel_resolution(self) -> float:
        return self._config.channel_resolution

    @property
    @override
    def integration_period(self) -> float:
        return self._config.integration_period

    @property
    @override
    def visibility_epoch(self) -> float:
        return self._config.visibility_epoch

    @override
    async def data(self) -> AsyncIterable[CorrelatedDatum]:
        shape = (
            self.num_baselines,
            self.channels.count,
            self._config.num_polarisations,
        )
        vis = np.zeros(shape, dtype=np.dtype(self._config.dtype))
        weights = np.ones((self.num_baselines, self.channels.count))

        for i in range(self._config.num_timesteps):
            timestamp = self.visibility_epoch + i * self._config.visibility_interval
            yield CorrelatedDatum(timestamp, vis, weights)
