"""Provides functionality for a measurement set data source."""

import asyncio
import copy
import dataclasses
import logging
from pathlib import Path
from typing import AsyncIterable

import numpy as np
from overrides import override
from realtime.receive.core.baseline_utils import baselines
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.common import autocast_fields
from realtime.receive.core.msutils import (
    MeasurementSet,
    calc_baselines,
    ms_timestamp_rows,
    vis_mjd_epoch,
)
from realtime.receive.core.scan import ScanType, SpectralWindow
from realtime.receive.core.time_utils import mjd_to_unix

from ska_sdp_cbf_emulator.resource_utils import fetch_resource

from .correlated_data_source import CorrelatedDataSource, CorrelatedDatum

logger = logging.getLogger(__name__)


@dataclasses.dataclass
@autocast_fields
class MeasurementSetDataSourceConfig:
    """Configuration for :class:`MeasurementSetDataSource`"""

    location: str
    """
    A URL or path to the measurement set or a .tar.gz containing a
    measurement set to read visibilities from.
    If a URL, it will be downloaded to a temporary directory first.
    """

    start_channel: int = 0
    """
    The first channel_id that is sent along with the MS visibilities.
    """

    channel_stride: int = 1
    """
    The gap between each channel_id in the sent visibilities.
    """

    num_repeats: int = 1
    """
    Number of times the number of timestamps are sent. This will send the same
    data over and over which is less realistic but imposes less stress on the
    file-system. `TIME` values increment with each repetition.
    """

    num_timestamps: int = 0
    """Number of timestamps to be sent, defaults to 0 which is all of them."""

    def __post_init__(self):
        if self.num_repeats <= 0:
            raise ValueError(f"num_repeats must be > 0: {self.num_repeats}")
        if self.num_timestamps == 1 and self.num_repeats > 1:
            raise ValueError(
                "repeating a single timestamp is not recommended, "
                "increase timestamps to at least 2"
            )

    def create_channel_range(self, ms: MeasurementSet):
        """
        Create the channel range that is appropriate to read from the
        specified measurement set, raising a ValueError if this isn't
        possible.
        """
        return ChannelRange(self.start_channel, ms.num_channels, self.channel_stride)


class MeasurementSetDataSource(CorrelatedDataSource):
    """A correlated data source reading from a Measurement Set"""

    def __init__(self, config: MeasurementSetDataSourceConfig) -> None:
        super().__init__()
        # TODO(yan-1550): Doesn't need to be a copy when we have a frozen dataclass
        self._config = copy.copy(config)
        self._ms_path: Path | None = None
        self._ms: MeasurementSet | None = None
        self._scan_type: ScanType | None = None

    @override
    async def __aenter__(self):
        self._ms_path = await fetch_resource(self._config.location)
        self._ms = MeasurementSet.open(str(self._ms_path))
        self._scan_type = self._ms.calculate_scan_type()
        return self

    @override
    async def __aexit__(self, exc_type, exc_value, traceback):
        self._ms.close()
        self._ms = None
        self._ms_path = None
        self._scan_type = None

    @property
    @override
    def name(self):
        return "Measurement Set"

    @property
    @override
    def info(self) -> list[tuple[str, str]]:
        num_stations = self._ms.num_stations
        num_baselines = self.num_baselines

        if num_baselines == baselines(num_stations, False):
            logger.warning("Baseline count indicates AUTO are not present")
        elif num_baselines == baselines(num_stations, True):
            logger.info("AUTOs present")

        return [
            ("no. stations", num_stations),
            ("no. baselines", num_baselines),
            ("channel range", self.channels),
            ("no. repeats", self._config.num_repeats),
        ]

    @property
    @override
    def num_baselines(self) -> int:
        return calc_baselines(self._ms)

    @property
    @override
    def channels(self) -> ChannelRange:
        return self._config.create_channel_range(self._ms)

    @property
    @override
    def channel_frequencies(self) -> np.ndarray:
        return self._spectral_window.frequencies

    @property
    @override
    def channel_resolution(self) -> float:
        return self._spectral_window.channel_bandwidth

    @property
    def _spectral_window(self) -> SpectralWindow:
        beam = self._scan_type.beams[0]
        return beam.channels.spectral_windows[0]

    @property
    @override
    def integration_period(self) -> float:
        exposures = self._ms.t.getcol("EXPOSURE")
        return np.mean(exposures)

    @property
    @override
    def visibility_epoch(self) -> float:
        return mjd_to_unix(vis_mjd_epoch(self._ms))

    @override
    async def data(self) -> AsyncIterable[CorrelatedDatum]:
        loop = asyncio.get_running_loop()

        repeat_idx = 0
        for repeat_idx in range(0, self._config.num_repeats):
            vis_iter = ms_timestamp_rows(
                self._ms,
                num_timestamps=self._config.num_timestamps,
                repeat_idx=repeat_idx,
            )

            for mjd_timestamp, row_idx, row_count in vis_iter:
                unix_timestamp = mjd_to_unix(mjd_timestamp)

                # Read all channels here, we'll slice as needed in the transmitter streams
                vis_amps = await loop.run_in_executor(
                    None, self._ms.read_column, "DATA", row_idx, row_count
                )

                weights = await self._get_weights(row_idx, row_count)
                if np.any(weights < 0.0) or np.any(weights > 1.0):
                    raise ValueError("CBF Emulator only supports normalised weights")

                yield CorrelatedDatum(unix_timestamp, vis_amps, weights)

    async def _get_weights(self, row_idx: int, row_count: int):
        loop = asyncio.get_running_loop()

        if "WEIGHT_SPECTRUM" in self._ms.t.colnames():
            # Optional column with shape (n_baselines, n_channels, n_pol)
            ms_weight_spectrums = await loop.run_in_executor(
                None, self._ms.read_column, "WEIGHT_SPECTRUM", row_idx, row_count
            )

            # Reduce to (n_baselines, n_channels)
            raw_weights = np.mean(ms_weight_spectrums, axis=2)
        else:
            # Fallback, with shape (n_baselines, n_pol)
            ms_weights = await loop.run_in_executor(
                None, self._ms.read_column, "WEIGHT", row_idx, row_count
            )

            # Reduce, then broadcast to (n_baselines, n_channels)
            raw_weights = np.broadcast_to(
                np.mean(ms_weights, axis=1)[..., np.newaxis],
                ms_weights.shape[:-1] + (self.channels.count,),
            )

        return raw_weights
