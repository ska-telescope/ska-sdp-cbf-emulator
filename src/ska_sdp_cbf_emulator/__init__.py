# -*- coding: utf-8 -*-

"""Module init code."""

__version__ = "7.0.0"
__author__ = "Stephen Ord"
__email__ = "stephen.ord@csiro.au"
