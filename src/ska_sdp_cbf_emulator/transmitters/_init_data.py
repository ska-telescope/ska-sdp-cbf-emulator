"""Module for initialisation of streams"""

from dataclasses import dataclass

import numpy as np
from realtime.receive.core.channel_range import ChannelRange


@dataclass(frozen=True)
class TransmitterLowInitData:
    """Additional data required to initialise a transmitter using the LOW ICD"""

    subarray_id: int
    """The subarray id"""

    beam_id: int
    """The station beam id"""

    zoom_window_id: int
    """The zoom window id"""

    integration_period_s: float
    """The amount of time in each integration interval"""

    channel_frequencies_hz: np.ndarray
    """The frequencies in Hz of each channel"""

    channel_resolution_hz: float
    """The resolution in Hz of every channel"""

    visibility_epoch: float
    """The unix timestamp of earliest visibility data"""

    def __post_init__(self):
        if self.beam_id < 1:
            raise ValueError("beam_id must be positive integer")


@dataclass(frozen=True)
class TransmitterInitData:
    """
    Encapsulates the data required to initialise a transmitter (and by
    extension, any stream(s) it sets up).
    """

    scan_id: int
    """The Scan ID to use for all payloads in transmission."""

    num_baselines: int
    """The number of baselines in the visibility data"""

    channels: ChannelRange
    """
    The channel ids that will be sent alongside visibility data.
    `ChannelRange.count` must correspond with the channel (second) dimension
    in `CorrelatedDatum.visibilities` and `CorrelatedDatum.normalised_weights`.
    """

    low: TransmitterLowInitData
    """Additional information required to initialise when emulating LOW"""

    hardware_id: int = 0xBEEF
    """The hardware id of the emulator"""

    def __post_init__(self):
        if (self.channels.count,) != self.low.channel_frequencies_hz.shape:
            raise ValueError("Channel count must match number of channel frequencies")
