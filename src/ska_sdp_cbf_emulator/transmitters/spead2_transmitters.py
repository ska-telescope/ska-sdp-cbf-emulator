# -*- coding: utf-8 -*-
"""
Implementation for the SPEAD2 network transport

This module contains the logic to take ICD Payloads and transmit them using
the SPEAD protocol.
"""
import asyncio
import dataclasses
import enum
import logging
import math
import time
import warnings
from contextlib import AbstractAsyncContextManager
from functools import cached_property
from typing import Iterator, Optional, Tuple

import numpy as np
import spead2.send.asyncio
from overrides import override
from realtime.receive.core import socket_utils
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.common import autocast_fields
from realtime.receive.core.icd import ICD, ItemID, LowICD, MidICD, Payload, Telescope

import ska_sdp_cbf_emulator
from ska_sdp_cbf_emulator.data_source.correlated_data_source import CorrelatedDatum
from ska_sdp_cbf_emulator.transmitters import Config, TransmitterInitData

logger = logging.getLogger(__name__)


def _version_as_u32(semantic_version: str):
    core_version = str.split(semantic_version, "-")[0]
    major, minor, patch = map(int, core_version.split("."))
    return major << 16 | minor << 8 | patch


async def _create_stream(thread_pool, endpoint, config, buffer_size, transport_proto):
    kwargs = {
        "thread_pool": thread_pool,
        "config": config,
        "buffer_size": buffer_size,
        "endpoints": (endpoint,),
    }
    if transport_proto == TransportProtocol.TCP:
        return await spead2.send.asyncio.TcpStream.connect(**kwargs)
    return spead2.send.asyncio.UdpStream(**kwargs)


def parse_endpoints(endpoints_spec):
    """
    Parse endpoint specifications.

    Each endpoint is a colon-separated host and port pair, and multiple
    endpoints are separated by commas. A port can be a single number or a range
    specified as "start-end", both inclusive.
    """
    endpoints = []
    for endpoint in endpoints_spec.split(","):
        host, port = endpoint.split(":")
        if "-" in port:
            start, end = map(int, port.split("-"))
            if start > end:
                raise ValueError(f"invalid port range: {start} > {end}")
            for port in range(start, end + 1):
                endpoints.append((host, port))
        else:
            endpoints.append((host, int(port)))
    return endpoints


class TransportProtocol(enum.Enum):
    """An enumeration of supported transport protocols for transmission."""

    TCP = "tcp"
    UDP = "udp"


@dataclasses.dataclass
@autocast_fields
# pylint: disable-next=too-many-instance-attributes
class Spead2TransmissionConfig(Config):
    """Configuration for transmission of data using ``spead2``"""

    method: str = "spead2_transmitters"

    telescope: Telescope = Telescope.MID
    """The telescope to emulate."""

    max_packet_size: int = 1472
    """The maximum size of packets to build, used by ``spead2``"""

    rate: float = 1024.0 * 1024.0 * 1024.0
    """
    The send data rate, in [bytes/s]. Declared as a float both for convenience
    (e.g. users can specify ``1e9`` instead of ``1000000000``) and backwards
    compatibility, but cast to an integer when used. ``0`` means send as fast
    as possible.
    """

    max_heaps: int = 1
    """The maximum number of heaps that can be in-flight."""

    num_streams: Optional[int] = None
    """
    Number of streams this sender should use to send data through. This option
    should be preferred over `channels_per_stream` (which is now deprecated).

    When the number of streams is more than the number of channels to send,
    this value is adjusted to be the same as the number of channels to avoid
    creating streams where data is never sent. Also, if this value is not an
    exact divisor of the number of channels, channels are distributed as evenly
    as possible across streams, with more channels assigned to the first
    streams in the list of streams.
    """

    channels_per_stream: int = 0
    """
    *DEPRECATED*, use `num_streams` instead.

    Number of channels for which data will be sent in a single stream. ``0``
    means to send all channels over one stream.
    """

    sender_threads: int = 1
    """Number of threads to use for network operations."""

    delay_start_of_stream_heaps: int = 0
    """
    The number of data heaps to send on each stream before sending the
    corresponding start-of-stream (SOS) heaps. 0 (default) means don’t delay
    the sending of the SOS heaps, < 0 means never send the SOS heaps. Note that
    non-zero values emulate out-of-order transmission for the SOS heaps.
    """

    endpoints: Optional[str] = None
    """
    The endpoints where data will be sent to. If present, ``target_host`` and
    ``target_port_start`` are ignored. If given, is a comma-separated list of
    endpoint specifications, where each endpoint specification takes the form
    of ``host:ports``, where ``ports`` is either a single number, or a range
    like ``start-end``. For example, ``127.0.0.1:8000,127.0.0.1:8001`` and
    ``127.0.0.1:8000-8001`` are equivalent.
    """

    target_host: Optional[str] = "127.0.0.1"
    """The host where data will be sent to."""

    target_port_start: Optional[int] = 41000
    """The first port where data will be sent to."""

    buffer_size: Optional[int] = None
    """
    The socket buffer size, used by ``spead2``. If not given, a default value
    is calculated based on the default value set by spead2, and the limits
    imposed by the OS.
    """

    transport_protocol: TransportProtocol = TransportProtocol.UDP
    """
    The network transport protocol used by ``spead2``.
    """

    def get_endpoints(self, num_streams) -> Iterator[Tuple[str, int]]:
        """An iterator over all endpoints, however they were defined by the user."""
        if self.endpoints:
            endpoints = parse_endpoints(self.endpoints)
            if len(endpoints) < num_streams:
                raise ValueError("missing endpoints for number of streams")
            yield from endpoints[:num_streams]
        else:
            for offset in range(num_streams):
                yield (self.target_host, self.target_port_start + offset)

    def __post_init__(self):
        if self.buffer_size is None:
            default_buffer_size = (
                spead2.send.UdpStream.DEFAULT_BUFFER_SIZE
                if self.transport_protocol == TransportProtocol.UDP
                else spead2.send.TcpStream.DEFAULT_BUFFER_SIZE
            )
            os_max_buffer_size = socket_utils.max_socket_write_buffer_size(
                self.transport_protocol.value
            )
            if os_max_buffer_size >= default_buffer_size:
                self.buffer_size = default_buffer_size
            else:
                logger.debug(
                    (
                        "Adjusting default sending buffer_size (%d -> %d) "
                        "to match OS max settings"
                    ),
                    default_buffer_size,
                    os_max_buffer_size,
                )
                self.buffer_size = os_max_buffer_size


# pylint: disable-next=too-many-instance-attributes
class Spead2SenderPayload(Payload):
    """SPEAD2 payload following the CSP-SDP interface document"""

    CBF_EMULATOR_FIRMWARE_VERSION = _version_as_u32(ska_sdp_cbf_emulator.__version__)
    LOW_CBF_SOURCE_ID = b"L"
    LOW_OUTPUT_RESOLUTION_BITS = 16

    # pylint: disable-next=too-many-arguments
    def __init__(
        self,
        telescope: Telescope,
        init_data: TransmitterInitData,
        first_channel_id: int,
        channel_idxs: slice,
    ):
        super().__init__()
        self._item_group = spead2.send.ItemGroup(flavour=spead2.Flavour(4, 64, 48, 0))
        self._telescope: Telescope = telescope
        self._init_data = init_data
        self.channel_idxs = channel_idxs
        self.channel_id = first_channel_id
        assert channel_idxs.start is not None, "channel_idxs.start must be provided"
        assert channel_idxs.step is None, "Non-contiguous slices are not supported"
        self.time_centroid_indices = np.zeros([self.channel_count, init_data.num_baselines])
        self.cci = np.zeros([self.channel_count, init_data.num_baselines])
        self._prepare_item_group()

    @property
    def _icd(self) -> ICD:
        return LowICD if self._telescope == Telescope.LOW else MidICD

    @property
    @override
    def scan_id(self):
        return self._init_data.scan_id

    @property
    @override
    def baseline_count(self):
        return self._init_data.num_baselines

    @property
    @override
    def hardware_id(self):
        return self._init_data.hardware_id

    @property
    @override
    def channel_count(self):
        return self.channel_idxs.stop - self.channel_idxs.start

    @cached_property
    def _low_icd_epoch(self):
        return LowICD.sps_to_icd_epoch(LowICD.unix_to_sps(self._init_data.low.visibility_epoch))

    def _prepare_item_group(self):
        """
        Adds all the required item descriptors to the internal ItemGroup and
        sets values on those that are known to stay fixed throughout a sending
        operation.
        """

        # Add item descriptors to internal item group
        if self._telescope == Telescope.LOW:
            if self.channel_count != 1:
                logger.warning(
                    "SKA Low dictates a single channel per stream, but %d are "
                    "being sent instead, this is non-standard.",
                    self._channel_count,
                )
                vis_shape = (self.channel_count, self.baseline_count)
            else:
                vis_shape = (self.baseline_count,)
        else:
            vis_shape = (self.channel_count, self.baseline_count)

        # Items is an enum.Enum, not an object instance
        # pylint: disable-next=invalid-name
        Items = self._icd.Items
        for item in Items:
            item_desc = item.value
            shape = tuple()
            dtype = item_desc.dtype
            if isinstance(dtype, tuple):
                dtype = list(dtype)
            if item_desc.id == ItemID.CORRELATOR_OUTPUT_DATA:
                shape = vis_shape
            self._item_group.add_item(
                id=item_desc.id,
                name=item_desc.name,
                description="",
                shape=shape,
                format=None,
                dtype=dtype,
            )

        # Set initial values
        corr_out_data = np.zeros(
            shape=vis_shape,
            dtype=list(Items.CORRELATOR_OUTPUT_DATA.value.dtype),
        )
        initial_item_group_values = (
            (Items.CHANNEL_ID, self.channel_id),
            (Items.BASELINE_COUNT, self.baseline_count),
            (Items.SCAN_ID, self.scan_id),
            (Items.HARDWARE_ID, self.hardware_id),
            (Items.CORRELATOR_OUTPUT_DATA, corr_out_data),
        )
        if self._telescope == Telescope.LOW:
            low_init_data = self._init_data.low

            def first_chan_data(data: np.ndarray):
                return data[self.channel_idxs][0]

            initial_item_group_values += (
                (Items.STATION_BEAM_ID, low_init_data.beam_id),
                (Items.SUBARRAY_ID, low_init_data.subarray_id),
                (Items.FREQUENCY_RESOLUTION, low_init_data.channel_resolution_hz),
                (Items.CHANNEL_FREQUENCY, first_chan_data(low_init_data.channel_frequencies_hz)),
                (Items.ZOOM_WINDOW_ID, low_init_data.zoom_window_id),
                (
                    Items.CBF_FIRMWARE_VERSION,
                    Spead2SenderPayload.CBF_EMULATOR_FIRMWARE_VERSION,
                ),
                (Items.CBF_SOURCE_ID, Spead2SenderPayload.LOW_CBF_SOURCE_ID),
                (
                    Items.OUTPUT_RESOLUTION,
                    Spead2SenderPayload.LOW_OUTPUT_RESOLUTION_BITS,
                ),
                (Items.INTEGRATION_PERIOD, low_init_data.integration_period_s),
                (Items.SPS_EPOCH, self._low_icd_epoch),
            )
        elif self._telescope == Telescope.MID:
            initial_item_group_values += (
                (Items.CHANNEL_COUNT, self.channel_count),
                (Items.POLARISATION_ID, self.polarisation_id),
                (Items.PHASE_BIN_ID, self.phase_bin_id),
                (Items.PHASE_BIN_COUNT, self.phase_bin_count),
            )
        self._set_item_group_values(initial_item_group_values)

    def _set_item_group_values(self, values):
        for item, value in values:
            self._item_group[item.value.id].value = value

    def get_data_heap(self):
        """Returns a data heap to be sent over the network."""

        # Update necessary Item Group values
        # Items is an enum.Enum, not an object instance
        # pylint: disable-next=invalid-name
        Items = self._icd.Items
        if self._telescope == Telescope.LOW:
            epoch_offset = LowICD.calc_icd_offset(
                LowICD.unix_to_sps(self.timestamp), self._low_icd_epoch
            )
            new_item_group_values = [
                (Items.EPOCH_OFFSET, epoch_offset),
            ]
        elif self._telescope == Telescope.MID:
            timestamp_count, timestamp_fraction = MidICD.unix_to_icd(self.timestamp)
            new_item_group_values = (
                (Items.TIMESTAMP_COUNT, timestamp_count),
                (Items.TIMESTAMP_FRACTION, timestamp_fraction),
                # in principle the rest shouldn't change, but in reality it
                # could (and we currently change some of these the *first* time
                # at least), so we better write the new values into the
                # ItemGroup
                (Items.BASELINE_COUNT, self.baseline_count),
                (Items.CHANNEL_COUNT, self.channel_count),
                (Items.CHANNEL_ID, self.channel_id),
                (Items.HARDWARE_ID, self.hardware_id),
                (Items.PHASE_BIN_ID, self.phase_bin_id),
                (Items.PHASE_BIN_COUNT, self.phase_bin_count),
                (Items.POLARISATION_ID, self.polarisation_id),
                (Items.SCAN_ID, self.scan_id),
            )
        else:
            raise RuntimeError(f"Invalid spead2 telescope: {self._telescope}")

        self._set_item_group_values(new_item_group_values)

        corr_out_data = self._item_group[ItemID.CORRELATOR_OUTPUT_DATA.value].value
        corr_out_data["TCI"] = self.time_centroid_indices
        corr_out_data["FD"] = self.correlated_data_fraction
        corr_out_data["VIS"] = self.visibilities
        if self._telescope == Telescope.MID:
            corr_out_data["CCI"] = self.cci

        # Create the heap and populate it with required Items
        heap = self._item_group.get_heap(descriptors="none", data="none")
        for item in self._icd.SENT_ON_DATA_HEAP:
            heap.add_item(self._item_group[item.id])
        return heap

    def get_start_heap(self):
        """Returns a start-of-stream heap to be sent over the network."""
        start_heap = self._item_group.get_start()

        # Add all Item Descriptors
        self._item_group.add_to_heap(start_heap, descriptors="all", data="none")

        # Add required Item values
        for item in self._icd.SENT_ON_SOS_HEAP:
            start_heap.add_item(self._item_group[item.id])

        return start_heap

    def get_end_heap(self):
        """Returns an end-of-stream heap to be sent over the network."""
        return self._item_group.get_end()


# pylint: disable-next=too-many-instance-attributes,invalid-name
class transmitter(AbstractAsyncContextManager):
    """
    SPEAD2 transmitter

    This class uses the spead2 library to transmit visibilities over multiple
    spead2 streams. Each visibility set given to this class' ``send`` method is
    broken down by channel range (depending on the configuration parameters),
    and each channel range is sent through a different stream.
    """

    config_class = Spead2TransmissionConfig

    def __init__(self, config: Spead2TransmissionConfig):
        self.config = config
        logger.info(
            "Creating StreamConfig with max_packet_size=%d",
            config.max_packet_size,
        )
        self.stream_config = spead2.send.StreamConfig(
            max_packet_size=config.max_packet_size,
            rate=int(config.rate),
            burst_size=10,
            max_heaps=config.max_heaps,
        )
        self.bytes_sent = 0
        self.heaps_sent = 0
        self.streams = []
        self._payloads: list[Spead2SenderPayload] = []
        self._channels: ChannelRange | None = None
        self._num_streams = 0
        self._start_heaps_sent = False

    @property
    def _stream_partitions(self):
        """
        Generate the first channel id for each stream and a slice that
        the stream can use to obtain the appropriate subset of the full
        channel visibilities to send.
        The assignment is done so that channels are evenly distributed in the
        case that num_channels is not a multiple of num_streams. Extra channels
        are assigned to the first streams on the list of streams.
        """
        channels_per_stream, leftover_channels = divmod(self._channels.count, self._num_streams)
        first_channel = self._channels.start_id
        start_idx = 0

        for stream_idx in range(self._num_streams):
            extra_channel = 1 if stream_idx < leftover_channels else 0
            num_channels = channels_per_stream + extra_channel
            yield first_channel, slice(start_idx, start_idx + num_channels)
            first_channel += num_channels * self._channels.stride
            start_idx += num_channels

    @property
    def channels_per_stream(self):
        """
        Utility property containing the number of channels sent in all streams.
        This is only valid if all streams send the same number of channels, so
        it must be used with care.
        """
        all_channels_counts = set(idxs.stop - idxs.start for _, idxs in self._stream_partitions)
        assert len(all_channels_counts) == 1
        return next(iter(all_channels_counts))

    @property
    def _num_channels(self):
        return self._channels.count

    async def prepare(self, init_data: TransmitterInitData):
        """Create the sending SPEAD streams"""

        start_time = time.time()
        self._channels = init_data.channels
        if self.config.num_streams is not None:
            self._num_streams = self.config.num_streams
            if self._num_streams > self._num_channels:
                logger.warning(
                    "Configured num_streams > num_channels, making num_streams = num_channels"
                )
                self._num_streams = self._num_channels
        elif self.config.telescope == Telescope.LOW:
            logger.info("Assuming 1 channel per stream for LOW")
            self._num_streams = self._num_channels
        else:
            warnings.warn(
                (
                    "transmission.channels_per_stream is deprecated, "
                    "use transmission.num_streams instead"
                ),
                DeprecationWarning,
            )
            if self.config.channels_per_stream == 0:
                self._num_streams = 1
            else:
                self._num_streams = math.ceil(self._num_channels / self.config.channels_per_stream)
        logger.info(
            "Preparing %d streams to send %d channels in total",
            self._num_streams,
            self._num_channels,
        )

        # Each stream uses a separate ItemGroup because Heaps created out of
        # ItemGroups can point to memory held by the ItemGroup; and since we
        # want different heaps sent through each of the streams we then need
        # independent ItemGroups
        self._payloads = [
            Spead2SenderPayload(
                self.config.telescope,
                init_data,
                first_channel_id,
                channel_idxs,
            )
            for first_channel_id, channel_idxs in self._stream_partitions
        ]

        # Create the streams; they still share a single I/O threadpool
        thread_pool = spead2.ThreadPool(threads=self.config.sender_threads)
        config = self.config
        transport_proto = config.transport_protocol
        for endpoint in config.get_endpoints(self._num_streams):
            host, port = endpoint
            logger.debug("Sending stream to %s:%d", host, port)
            stream = await _create_stream(
                thread_pool,
                endpoint,
                self.stream_config,
                config.buffer_size,
                transport_proto,
            )
            self.streams.append(stream)

        logger.info(
            "Created %d %s spead2 streams to send data for %d channels in %.3f [ms]",
            self._num_streams,
            transport_proto.value.upper(),
            self._channels.count,
            (time.time() - start_time) * 1000,
        )

    def _should_send_start_of_stream_heaps(self):
        if self.config.delay_start_of_stream_heaps < 0 or self._start_heaps_sent:
            return False
        if self.config.delay_start_of_stream_heaps == 0:
            return True
        data_heaps_sent_per_stream = self.heaps_sent // len(self.streams)
        return data_heaps_sent_per_stream >= self.config.delay_start_of_stream_heaps

    async def _maybe_send_start_of_stream_heap(self):
        if self._should_send_start_of_stream_heaps():
            await self._send_heaps([payload.get_start_heap() for payload in self._payloads])
            self._start_heaps_sent = True

    async def _send_heaps(self, heaps):
        assert len(heaps) == len(self.streams)
        send_operations = []
        for heap, stream in zip(heaps, self.streams):
            send_operations.append(stream.async_send_heap(heap))
        results = await asyncio.gather(*send_operations)
        self.bytes_sent += sum(results)
        self.heaps_sent += len(heaps)

    async def send(self, datum: CorrelatedDatum):
        """
        Send a visibility set through all SPEAD2 streams

        :param timestamp: the visibilities' timestamp as a UNIX timestamp.
        :param vis: the visibilities, in ICD order, assumed to start from channel_id=0
        :param corr_data_frac: the correlated fraction of the visibilities, in ICD order
        """
        await self._maybe_send_start_of_stream_heap()
        logger.debug("Sending heaps to %d spead2 streams", len(self.streams))
        heaps = []
        assert len(self._payloads) == len(self.streams)
        for payload_idx, payload in enumerate(self._payloads, start=1):
            # When sending to multiple streams (e.g., 96) we could spend a long
            # time in this loop without yielding control back, so let's do that
            if payload_idx % 20 == 0:
                await asyncio.sleep(0)

            payload.timestamp = datum.unix_timestamp
            payload.visibilities = datum.icd_visibilities[payload.channel_idxs]
            payload.correlated_data_fraction = datum.icd_corr_data_frac[payload.channel_idxs]

            heaps.append(payload.get_data_heap())
        await self._send_heaps(heaps)

    async def close(self):
        """Sends the end-of-stream message"""
        await self._maybe_send_start_of_stream_heap()
        await self._send_heaps([payload.get_end_heap() for payload in self._payloads])

    @override
    async def __aenter__(self):
        return self

    @override
    async def __aexit__(self, ext_type, exc, tb):
        await self.close()
