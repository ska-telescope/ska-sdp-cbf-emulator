"""Dynamic loading of transmitters"""

import importlib
import logging

from realtime.receive.core.common import from_dict

from ._config import Config
from ._init_data import TransmitterInitData

logger = logging.getLogger(__name__)


def _maybe_upgrade_config(config, config_class, base_config_class):
    if isinstance(config, config_class):
        return config
    assert base_config_class in config_class.mro()
    return config_class()


def _get_transmitter_class(config: Config):
    method = config.method
    try:
        module = importlib.import_module(".." + method, package=__name__)
        classname = "transmitter"
    except ImportError:
        modname, classname = method.rsplit(".", 1)
        module = importlib.import_module(modname)
    return getattr(module, classname)


def create_config(**kwargs):
    """
    Creates and populates a Config class.

    The returned object is not necessarily a Config class directly, but can be
    one of the subclasses that represent a configuration for a specific
    transmitter. To achieve this, a two-step process takes place: first a base
    Config object is created using kwargs. This base Config object knows the
    transmission method that shall be used, and thus we load the module/class
    associated to the method. In the second step we query the transmission
    class for its configuration class, and construct one with kwargs, which is
    what we finally return to the user
    """
    generic_config = from_dict(Config, kwargs)
    return from_dict(_get_transmitter_class(generic_config).config_class, kwargs)


async def create(config: Config, init_data: TransmitterInitData):
    """
    Creates a transmitter object based on ``config`` that will transmit data
    for the given init data.
    """
    transmitter_class = _get_transmitter_class(config)
    config = _maybe_upgrade_config(config, transmitter_class.config_class, Config)
    logger.info("Creating transmitter with config: %r", config)
    transmitter = transmitter_class(config)

    await transmitter.prepare(init_data)
    return transmitter
