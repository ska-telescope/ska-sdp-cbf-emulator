# pylint: disable=missing-module-docstring
import dataclasses


@dataclasses.dataclass
class Config:
    """Base configuration for all transmitters"""

    method: str = "spead2_transmitters"
    """The method used to transmit"""
