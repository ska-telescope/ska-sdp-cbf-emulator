"""Run the cbf-emulator driven by Configure/Scan commands sent to SDP."""

import asyncio
import concurrent.futures
import logging
import threading
from dataclasses import dataclass
from typing import Dict, Optional

import ska_sdp_config
from ska_sdp_config.config import Transaction

logger = logging.getLogger(__name__)

READY_SCAN_ID = 0  # scan ID of the ready state


@dataclass(frozen=True)
class ScanTarget:
    """The target endpoint associated to a given scan ID"""

    scan_id: int
    target_host: str
    target_port: int


def _calculate_target(receive_addresses, scan_type_id: str) -> tuple[str, int]:
    """
    Determines the receive target transmission config from the
    receiveAddresses subarray response.
    """
    # Using address of first beam only, 'endpoints' option may specify
    # multiple targets
    scan_type_addresses = receive_addresses[scan_type_id]
    first_beam_id, first_beam_addresses = next(iter(scan_type_addresses.items()))
    logger.info("using target address of %s %s", scan_type_id, first_beam_id)
    return (
        first_beam_addresses["host"][0][1],
        first_beam_addresses["port"][0][1],
    )


class SdpConfigScanMonitor:
    """
    Watches and enqueues a sequence SDP Configuration Database
    Execution Block scan changes to be processed by calling
    :meth:`pop_scan_target`.
    """

    def __init__(
        self,
        execution_block_id: str,
        sdp_config: ska_sdp_config.Config,
    ):
        self._sdp_config = sdp_config
        self._execution_block_id = execution_block_id
        self._target: Dict[str, str] = {}
        self._loop: Optional[asyncio.AbstractEventLoop] = None
        self._watcher = None
        self._watcher_lock = threading.Lock()
        self._scan_target_queue: asyncio.Queue | None = None

    async def pop_scan_target(self) -> Optional[ScanTarget]:
        """
        Asynchronous interface to get the next scan target when it changes.

        Returns:
            Optional[ScanTarget]: scan_id and target
            transmission config.

            A scan_id of 0 is a signal to stop scanning. None is returned
            when monitoring has ended.
        """
        return (
            await self._scan_target_queue.get() if self._loop and self._scan_target_queue else None
        )

    async def arun(self):
        """
        Asynchronous interface to start monitoring of scan changes. Will
        run until the configured execution block no longer exists.
        """
        try:
            # NOTE: python <3.9 requires asyncio.Queue be initialized with
            # a running async loop avaialable
            self._scan_target_queue = asyncio.Queue(maxsize=2)
            executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
            self._loop = asyncio.get_running_loop()
            await self._loop.run_in_executor(executor, self._run)
        except asyncio.CancelledError:
            self._stop()

    def _put_scan_target(self, *scan_target_args):
        assert self._loop is not None
        if not scan_target_args:
            scan_target = None
        else:
            scan_target = ScanTarget(*scan_target_args)
        asyncio.run_coroutine_threadsafe(self._scan_target_queue.put(scan_target), self._loop)

    def _signal_end_of_monitoring(self):
        self._put_scan_target()

    def _run(self):
        try:
            self._monitor()
        except asyncio.CancelledError:
            # avoid creating new async tasks when cancelled
            pass
        except StopIteration:
            logger.info("execution block ended, ending monitoring...")
            self._signal_end_of_monitoring()
        except Exception:
            logger.info("unexpected monitor exception, ending monitoring...")
            self._signal_end_of_monitoring()
            raise
        logger.info("monitor finished")

    def _monitor(self):
        logger.info("Monitoring sdp scans")
        last_scan_id: Optional[int] = None
        # NOTE: threading needed as watcher uses blocking call
        # instead of 'async for'
        for watcher in self._sdp_config.watcher():
            with self._watcher_lock:
                self._watcher = watcher
            if not self._loop:
                raise asyncio.CancelledError("monitor was stopped")
            for txn in watcher.txn():
                assert isinstance(txn, Transaction)
                # pylint: disable-next=invalid-name

                eb = txn.execution_block.get(self._execution_block_id)
                if eb is None:
                    if last_scan_id is None:
                        logger.error(
                            "Execution Block with ID %s was not found",
                            self._execution_block_id,
                        )
                        raise RuntimeError(
                            f"Execution Block with ID {self._execution_block_id} was not found"
                        )
                    raise StopIteration(
                        f"Execution Block with ID {self._execution_block_id} was ended"
                    )
                eb_state = txn.execution_block.state(self._execution_block_id).get()
                if eb_state is None:
                    raise RuntimeError(
                        f"Execution Block State with ID {self._execution_block_id} not found"
                    )

                scan_id: int = eb_state["scan_id"] if eb_state["scan_id"] is not None else 0
                scan_type_id: str = (
                    eb_state["scan_type"] if eb_state["scan_type"] is not None else ""
                )
                logger.debug("got scan type: %s, scan_id: %s", scan_type_id, scan_id)

                if scan_id != last_scan_id:
                    logger.info("Detected new scan %s", scan_id)
                    last_scan_id = scan_id
                    if scan_id != READY_SCAN_ID:
                        pb_id = eb_state["pb_receive_addresses"]
                        receive_addresses = txn.processing_block.state(pb_id).get()[
                            "receive_addresses"
                        ]
                        target_host, target_port = _calculate_target(
                            receive_addresses, scan_type_id
                        )
                    else:
                        target_host, target_port = ("", 0)
                    self._put_scan_target(scan_id, target_host, target_port)
        self._signal_end_of_monitoring()

    def _stop(self):
        """
        Must be called to end the background thread early.
        """
        self._loop = None
        with self._watcher_lock:
            self._watcher.trigger()
