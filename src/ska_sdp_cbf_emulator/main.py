# -*- coding: utf-8 -*-
"""
Emulator sender's main entry point.

This module contains the main entry point for sending data over the network
emulating the CBF, the packetise coroutine. When using inside your program
care must be taken to avoid hanging the thread.
"""

import argparse
import asyncio
import logging
import signal
import sys
import warnings
from contextlib import suppress

import ska_ser_logging
import yaml
from realtime.receive.core.config import augment_config

from ska_sdp_cbf_emulator.emulator import arun_cbf_emulator
from ska_sdp_cbf_emulator.packetiser import packetise, to_sender_config

logger = logging.getLogger(__name__)


def _setup_async_graceful_termination(
    signals=(signal.SIGINT, signal.SIGTERM),
):
    """
    Gracefully handles shutdown signals by cancelling all pending tasks.
    Can only be called from an async function.
    """

    async def _shutdown(sig: signal.Signals):
        logger.info("handling %s", sig)
        for task in [task for task in asyncio.all_tasks() if task is not asyncio.current_task()]:
            task.cancel()

    loop = asyncio.get_running_loop()
    for signame in signals:
        loop.add_signal_handler(
            signame,
            lambda signame=signame: asyncio.create_task(_shutdown(signame)),
        )


def _config_from_yaml(config_file_path: str):
    if not config_file_path:
        return {}
    with open(config_file_path, "rb") as config_file:
        return yaml.load(config_file, yaml.SafeLoader)


async def amain():
    """Main coroutine for emu-send command-line tool."""
    _setup_async_graceful_termination()

    if not sys.warnoptions:
        warnings.simplefilter("always", DeprecationWarning)

    parser = argparse.ArgumentParser(description="Creates SPEAD2 heaps out of a MS file")
    parser.add_argument(
        "-eb",
        "--execution_block_id",
        help="If set, will monitor the SDP Configuration Database for scan changes",
    )
    parser.add_argument(
        "-c",
        "--config",
        help="The configuration file to load (YAML format), default is empty.",
        default="",
        type=_config_from_yaml,
    )
    parser.add_argument(
        "-o",
        "--option",
        help="Additional configuration options in the form of category.name=value",
        action="append",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If set, more verbose output will be produced",
        action="store_true",
    )
    parser.add_argument(
        "-q",
        "--quiet",
        help="If set, less verbose output will be produced",
        action="store_true",
    )
    parser.add_argument(
        "measurement_set",
        nargs="?",
        help="(DEPRECATED) The measurement set to read data from",
    )

    args = parser.parse_args()
    logging_level = (
        logging.DEBUG if args.verbose else (logging.WARNING if args.quiet else logging.INFO)
    )
    ska_ser_logging.configure_logging(level=logging_level)
    config = args.config
    if args.option:
        augment_config(config, args.option)

    if "reader" in config:
        warnings.warn("The `reader` config path has been renamed to `ms`", DeprecationWarning)
        config["ms"] = config.pop("reader")

    if args.measurement_set:
        warnings.warn(
            "Command-line measurement set input has been deprecated."
            "Please use `reader.ms` instead.",
            DeprecationWarning,
        )
        if "ms" not in config:
            config["ms"] = {}
        config["ms"]["location"] = args.measurement_set

    sender_config = to_sender_config(config)
    with suppress(asyncio.CancelledError):
        if not args.execution_block_id:
            await packetise(sender_config)
        else:
            await arun_cbf_emulator(args.execution_block_id, sender_config)


def main():
    """Runs amain() in a new loop."""
    asyncio.run(amain())


if __name__ == "__main__":
    main()
