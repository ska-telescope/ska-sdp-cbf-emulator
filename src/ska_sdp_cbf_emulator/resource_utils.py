"""
Utilities for managing filesystem resources.
"""

import logging
import os.path
import tarfile
import tempfile
import urllib.parse
from pathlib import Path
from typing import Union

import aiofiles
import aiohttp

logger = logging.getLogger(__name__)


def untar(archive_path: Union[str, Path]) -> Path:
    """Extracts a tar archive to the same directory as the archive.

    Returns:
        Path: string name of the extracted directory
    """
    archive_path = Path(archive_path)
    with tarfile.open(archive_path, "r:gz") as tar:
        tar.extractall(archive_path.parent)
        return archive_path.parent / tar.getnames()[0]


async def fetch_resource(resource_url: str, temp_dir: str = tempfile.gettempdir()) -> Path:
    """
    Returns a filesystem path from a file resource url. If a url scheme
    is provided the resource will be downloaded to a temporary directory
    and extracted if it is an archive.
    Args:
        resource_url (str): file or archive resource url

    Raises:
        ValueError: raised if the resource location does not exist

    Returns:
        Path: verified filesystem path to the fetched data
    """
    parse_result = urllib.parse.urlparse(resource_url)
    if parse_result.scheme:
        # assume string is download url
        async with aiohttp.ClientSession() as session:
            url = parse_result.path
            file_name = Path(temp_dir) / urllib.parse.unquote(Path(url).name)
            if not os.path.exists(file_name):
                logger.info("Downloading resource url %s to %s", url, file_name)
                async with session.get(parse_result.geturl()) as resource:
                    async with aiofiles.open(file_name, "wb") as output_file:
                        async for data in resource.content.iter_any():
                            await output_file.write(data)
            else:
                logger.info("Using cached resource at %s", file_name)
    else:
        # assume string is filesystem path
        if not os.path.exists(resource_url):
            raise ValueError(f"Filesystem resource {resource_url} does not exist")
        file_name = Path(resource_url)

    # extract if resource is an archive
    return untar(file_name) if file_name.suffix.endswith((".tar", ".gz")) else file_name
