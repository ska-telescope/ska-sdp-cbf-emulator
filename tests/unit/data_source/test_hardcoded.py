import time

import numpy as np
import pytest
from aiostream import stream
from realtime.receive.core.channel_range import ChannelRange

from ska_sdp_cbf_emulator.data_source import HardcodedDataSource, HardcodedDataSourceConfig


@pytest.mark.parametrize(
    "num_stations, include_autos, expected_num_baselines",
    [
        (3, False, 3),
        (3, True, 6),
    ],
)
def test_passthrough_properties_are_correct(
    num_stations: int,
    include_autos: bool,
    expected_num_baselines: int,
):
    config = HardcodedDataSourceConfig(
        num_stations=num_stations,
        include_autocorrelations=include_autos,
        channels=ChannelRange(2, 5, 3),
        visibility_epoch=time.time(),
        num_timesteps=1,
        first_channel_freq=10,
        channel_resolution=0.5,
        integration_period=0.3,
    )
    source = HardcodedDataSource(config)

    assert expected_num_baselines == source.num_baselines
    assert config.channels == source.channels
    assert config.visibility_epoch == source.visibility_epoch
    assert config.first_channel_freq == source.channel_frequencies[0]
    assert config.channel_resolution == source.channel_resolution
    assert config.integration_period == source.integration_period


@pytest.mark.asyncio
async def test_correct_visibilities_are_generated():
    config = HardcodedDataSourceConfig(
        num_stations=3,
        include_autocorrelations=True,
        channels=ChannelRange(1, 3, 2),
        num_timesteps=5,
        num_polarisations=4,
        visibility_epoch=time.time(),
        visibility_interval=1.0,
    )
    source = HardcodedDataSource(config)

    data = await stream.list(source.data())
    assert config.num_timesteps == len(data)

    timestamps = [tv.unix_timestamp for tv in data]
    assert config.visibility_epoch == timestamps[0]
    ts_diffs = [timestamps[i + 1] - timestamps[i] for i in range(len(timestamps) - 1)]
    assert len(set(ts_diffs)) == 1
    assert ts_diffs[0] == 1.0

    visibilities = [tv.visibilities for tv in data]
    shapes = set(v.shape for v in visibilities)
    assert len(shapes) == 1
    assert next(iter(shapes)) == (6, 3, 4)
    assert np.all(np.array(visibilities) == 0)
