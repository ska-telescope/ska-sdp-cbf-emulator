import shutil
import tempfile

import numpy as np
import pytest
import pytest_asyncio
from casacore import tables
from realtime.receive.core.msutils import MeasurementSet, Mode

from ska_sdp_cbf_emulator.data_source.measurement_set import (
    MeasurementSetDataSource,
    MeasurementSetDataSourceConfig,
)

NUM_TIMESTEPS = 133
SPECTRUM_WEIGHT = 0.87


@pytest_asyncio.fixture(name="data_source")
@pytest.mark.asyncio
async def measurement_set_data_source_fixture():
    config = MeasurementSetDataSourceConfig(location="data/sim-vis.ms")
    async with MeasurementSetDataSource(config) as data_source:
        yield data_source


@pytest_asyncio.fixture(name="ws_data_source")
@pytest.mark.asyncio
async def weight_spectrum_measurement_set_data_source_fixture():
    with tempfile.TemporaryDirectory(suffix=".ms") as ms_dir:
        shutil.copytree("data/sim-vis.ms", ms_dir, dirs_exist_ok=True)

        with MeasurementSet.open(ms_dir, Mode.READWRITE) as ms:
            ms.t.addcols(
                tables.makearrcoldesc(
                    "WEIGHT_SPECTRUM",
                    0.0,
                    valuetype="double",
                    shape=(ms.num_channels, ms.num_pols),
                    datamanagertype="TiledColumnStMan",
                )
            )

            weight_spectrums = np.broadcast_to(
                SPECTRUM_WEIGHT,
                (ms.num_rows, ms.num_pols, ms.num_channels),
            )
            ms.t.putcol("WEIGHT_SPECTRUM", weight_spectrums)

        config = MeasurementSetDataSourceConfig(location=ms_dir)
        async with MeasurementSetDataSource(config) as data_source:
            yield data_source


@pytest.mark.asyncio
async def test_data_is_read_correctly(data_source: MeasurementSetDataSource):
    assert 4 == data_source.channels.count
    assert (4,) == data_source.channel_frequencies.shape
    assert data_source.channel_resolution > 0.0
    assert data_source.integration_period > 0.0

    timestep_count = 0
    async for datum in data_source.data():
        timestep_count += 1
        assert np.all(datum.visibilities != 0.0)
        assert np.all(datum.normalised_weights > 0.0)

    assert timestep_count == NUM_TIMESTEPS


@pytest.mark.asyncio
async def test_weight_spectrums_are_read_if_available(ws_data_source: MeasurementSetDataSource):
    timestep_count = 0

    async for datum in ws_data_source.data():
        timestep_count += 1
        assert np.all(datum.normalised_weights == SPECTRUM_WEIGHT)

    assert timestep_count == NUM_TIMESTEPS
