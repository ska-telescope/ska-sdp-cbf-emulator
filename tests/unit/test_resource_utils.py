"""Tests for resource_utils module"""

import asyncio
import subprocess
import tempfile

import pytest
from realtime.receive.core import msutils

from ska_sdp_cbf_emulator.resource_utils import fetch_resource

PORT = 8000


async def _wait_for_server():
    tries = 30
    sleep_interval = 0.1
    while tries:
        try:
            await asyncio.open_connection("127.0.0.1", PORT)
        except ConnectionRefusedError:
            await asyncio.sleep(sleep_interval)
            tries -= 1
        else:
            break
    assert tries


@pytest.fixture(name="data_server")
def _data_server_fixture():
    with subprocess.Popen(["python3", "-m", "http.server", str(PORT)]) as proc:
        try:
            asyncio.run(_wait_for_server())
            yield proc
        finally:
            proc.terminate()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "url",
    [
        "http://localhost:8000/data/sim-vis.ms.tar.gz",
        "http://localhost:8000/data/sim-vis.ms.tar.gz?inline=false",
        "data/sim-vis.ms.tar.gz",
        "data/sim-vis.ms",
    ],
)
async def test_fetch_resource_ms(data_server, url: str):
    assert data_server
    with tempfile.TemporaryDirectory() as temp_dir:
        resource = await fetch_resource(url, temp_dir)
        ms = msutils.MeasurementSet.open(str(resource))
        assert ms.num_rows >= 0
