"""Tests for the emu-send command-line entrypoint."""

import asyncio
import re
import sys
import tempfile

import pytest
import yaml

INPUT_MS = "data/AA05LOW.ms.tar.gz"


async def _run_ms_sender_proc(*args):
    full_args = [
        "-o",
        "transmission.rate=0",
        "-o",
        f"ms.location={INPUT_MS}",
        "-o",
        "ms.interval=-1",
        "-o",
        "ms.repeats=1000",
    ]
    full_args += args

    await _run_sender_proc(*full_args)


async def _run_sender_proc(*args):
    full_args = [
        "-m",
        "ska_sdp_cbf_emulator.main",
    ]
    full_args += args

    sender_proc = await asyncio.create_subprocess_exec(
        sys.executable, stdout=asyncio.subprocess.PIPE, *full_args
    )

    async def _wait_for_sender_streams_created():
        streams_created_re = re.compile("Created .* spead2 streams")
        async for line in sender_proc.stdout:
            if streams_created_re.search(line.decode("utf8")):
                break

    try:
        await asyncio.wait_for(_wait_for_sender_streams_created(), timeout=20)
    finally:
        sender_proc.terminate()
        assert 0 == await sender_proc.wait()


@pytest.mark.parametrize("config", ({}, {"ms": {"num_repeats": 10}}))
@pytest.mark.asyncio
async def test_cli_with_config_file(config):
    with tempfile.NamedTemporaryFile("wt") as config_file:
        yaml.dump(config, config_file)
        config_file.flush()
        await _run_ms_sender_proc("-c", config_file.name)


@pytest.mark.asyncio
async def test_cli_without_config_file():
    await _run_ms_sender_proc()


@pytest.mark.asyncio
async def test_hardcoded_emulation():
    await _run_sender_proc(
        "-o",
        "hardcoded.num_stations=3",
        "-o",
        "hardcoded.channels=0:10",
        "-o",
        "hardcoded.num_timesteps=10",
    )


@pytest.mark.asyncio
async def test_deprecated_ms_input():
    await _run_sender_proc(INPUT_MS)


@pytest.mark.asyncio
async def test_deprecated_reader_input():
    await _run_sender_proc("-o", f"reader.location={INPUT_MS}")
