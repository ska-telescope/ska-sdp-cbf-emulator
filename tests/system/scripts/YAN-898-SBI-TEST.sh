#!/bin/bash

# This is a script that will run a test to enable the testing of resilience to packet loss.
# as part of SP-1857

TMP_DIR="/tmp/emu"
CONFIG="/tmp/emu/SP-1857.in"
OUTPUT="/tmp/emu/received.ms"
TOSEND="/tmp/emu/tosend.ms"
MODEL="/tmp/emu/model.json"

if [ -d $OUTPUT ]
then
  rm -r $OUTPUT
fi
if [ -d $TOSEND ]
then
  rm -r $TOSEND
fi
if [ -d $MODEL ]
then
  rm -r $MODEL
fi
if [ -d $CONFIG ]
then
  rm -r $CONFIG
fi



if [ -z "$1" ]
then
  echo "No file supplied please supply a measurement set to send."
  exit 1
else
  SEND_FILE="$1"
fi
if [ -z "$2" ]
then
  echo "No scheduling block instance supplied."
  exit 1
else
  SCHED_FILE="$2"
fi


mkdir -p /tmp/emu
cp -R $SEND_FILE $TOSEND
cp -R $SCHED_FILE $MODEL

cat > $CONFIG << EOF
[reception]
method = spead2_receivers
receiver_port_start = 41001
schedblock = ${MODEL}
ring_heaps = 128
outputfilename = ${OUTPUT}
consumer = plasma_writer
plasma_path = /tmp/plasma_socket

[transmission]
method = spead2_transmitters
target_host = 127.0.0.1
target_port_start = 41001
channels_per_stream = 1
rate = 1000000000
time_interval = 0

[reader]
num_repeats = 1
num_timestamps = 100
num_channels = 0

EOF

echo "Starting the test container"

docker stop emu-test
docker run -td --rm --name emu-test --cap-add=NET_ADMIN -v ${TMP_DIR}:${TMP_DIR} ska-sdp-cbf-emulator:recv 

echo "Install iproute2 for traffic shaping"

docker exec -t emu-test apt install -y iproute2

echo "Start the plasma store"

docker exec -td emu-test plasma_store -m 100000000 -s /tmp/plasma_socket

echo "Running Receiver in Background"

docker exec -td emu-test emu-recv -v -c $CONFIG

echo "Start the plasma writer"

docker exec -td emu-test plasma-mswriter -v -s  /tmp/plasma_socket --max_payloads 25 --timestamp_output true $OUTPUT

echo "Corrupting the UDP packet stream to the level of approximately 1%"
docker exec -t emu-test /sbin/tc qdisc add dev lo root netem corrupt 1%

#echo "Dropping 1% of the UDP packets"
#docker exec -t emu-test /sbin/tc qdisc add dev lo root netem loss 1%

#echo "A 1 second variable delay in the arrival of packets!
#docker exec -t emu-test /sbin/tc qdisc change dev l0 root netem delay 10ms 1000ms
echo "Running Sender in Foreground"

docker exec -t emu-test emu-send -vv -c $CONFIG /tmp/emu/tosend.ms

echo "Giving the receiver and plasma containers a couple of seconds to wrap up"

sleep 2

echo "Stopping test container"

docker stop emu-test
