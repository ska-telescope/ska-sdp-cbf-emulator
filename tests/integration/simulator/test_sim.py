import os
import subprocess
import unittest

docker_present = True
try:
    subprocess.check_call(["docker", "--version"])
except OSError:
    docker_present = False


@unittest.skipUnless(docker_present, "docker client not found")
def test_simulator():
    cwd = os.getcwd()
    project_dir = cwd + "/simulator/"
    os.chdir(str(project_dir))
    try:
        subprocess.check_call(["./run-sim.sh", "-c"])
    finally:
        os.chdir(cwd)
