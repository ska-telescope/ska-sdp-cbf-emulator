"""Module to test plugin transmitters."""

from dataclasses import dataclass

import pytest
from realtime.receive.core.common import autocast_fields

from ska_sdp_cbf_emulator import transmitters
from ska_sdp_cbf_emulator.transmitters import Config

from ...common import default_transmitter_init_data

METHOD = __name__ + ".Transmitter"

# pylint: disable=W,C,R


@dataclass
@autocast_fields
class MyTransmitterConfig(Config):
    method: str = METHOD
    a: int = 0
    b: str = "b"
    c: float = 2.0


class Transmitter:
    config_class = MyTransmitterConfig

    def __init__(self, _config):
        pass

    async def prepare(self, *args):
        pass


@pytest.mark.asyncio
async def test_load_plugin_transmitter():
    """
    Test transmitter factory can load plugin transmitters with their own configuration
    classes.
    """
    config = transmitters.create_config(method=METHOD, a=1, b="B", c="1.0")
    assert isinstance(config, MyTransmitterConfig)
    assert config.method == METHOD
    assert config.a == 1
    assert config.b == "B"
    assert config.c == 1.0
    transmitter = await transmitters.create(config, default_transmitter_init_data(4))
    assert isinstance(transmitter, Transmitter)
