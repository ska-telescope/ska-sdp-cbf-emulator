#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the spaed2_transmitters module."""
import asyncio
import contextlib
import time
import warnings

import numpy as np
import pytest
import spead2.recv.asyncio
from realtime.receive.core import ChannelRange
from realtime.receive.core.icd import ItemID, LowICD, MidICD, Telescope

from ska_sdp_cbf_emulator import transmitters
from ska_sdp_cbf_emulator.data_source.correlated_data_source import CorrelatedDatum
from ska_sdp_cbf_emulator.transmitters import TransmitterInitData
from ska_sdp_cbf_emulator.transmitters.spead2_transmitters import parse_endpoints
from tests.common import (
    create_tcp_recv_stream,
    default_transmitter_init_data,
    default_transmitter_low_init_data,
)

# pylint: disable=W,C,R


@contextlib.contextmanager
def warnings_as_errors():
    """Show python warnings as errors."""
    with warnings.catch_warnings() as catcher:
        warnings.simplefilter("error")
        yield catcher


async def _default_transmitter(config, channels: int | ChannelRange):
    return await transmitters.create(config, default_transmitter_init_data(channels))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    (
        "num_channels",
        "num_streams",
        "channels_per_stream",
        "expected_channels_per_stream",
    ),
    (
        (10, None, 2, 2),
        (10, None, 0, 10),
        (1000, None, 1000, 1000),
        (1000, None, 0, 1000),
        (10, 5, None, 2),
        (10, 2, None, 5),
        (1000, 1, None, 1000),
    ),
)
async def test_streams_specification(
    num_channels,
    num_streams,
    channels_per_stream,
    expected_channels_per_stream,
):
    ctx_manager = (
        warnings_as_errors() if num_streams is not None else pytest.warns(DeprecationWarning)
    )
    with ctx_manager:
        config = transmitters.create_config(
            num_streams=num_streams,
            channels_per_stream=channels_per_stream,
        )
        transmitter = await _default_transmitter(config, num_channels)
        async with transmitter:
            assert transmitter.channels_per_stream == expected_channels_per_stream


@pytest.mark.asyncio
@pytest.mark.parametrize("stride", [1, 2, 3])
@pytest.mark.parametrize("start_id", [0, 100])
@pytest.mark.parametrize(
    ("num_channels", "num_streams", "channel_assignment"),
    (
        (1, 2, ((0, 1),)),
        (3, 2, ((0, 2), (2, 1))),
        (5, 2, ((0, 3), (3, 2))),
        (7, 2, ((0, 4), (4, 3))),
        (4, 3, ((0, 2), (2, 1), (3, 1))),
        (5, 3, ((0, 2), (2, 2), (4, 1))),
        (7, 3, ((0, 3), (3, 2), (5, 2))),
        (7, 4, ((0, 2), (2, 2), (4, 2), (6, 1))),
        (7, 5, ((0, 2), (2, 2), (4, 1), (5, 1), (6, 1))),
        (7, 6, ((0, 2), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1))),
    ),
)
async def test_uneven_channel_distribution(
    start_id, num_channels, stride, num_streams, channel_assignment
):
    """
    Tests that channels are correctly distributed across streams when the
    number of streams is not an exact divisor of the number of channels.
    """
    config = transmitters.create_config(num_streams=num_streams)
    transmitter = await _default_transmitter(config, ChannelRange(start_id, num_channels, stride))
    async with transmitter:
        assert np.array_equal(
            list(transmitter._stream_partitions),
            [
                (
                    start_id + assigned_start_idx * stride,
                    slice(assigned_start_idx, assigned_start_idx + assigned_count),
                )
                for assigned_start_idx, assigned_count in channel_assignment
            ],
        )


async def send_single_heap(
    num_streams,
    epoch=time.time(),
    channels: ChannelRange | None = None,
    channel_vis: np.ndarray | None = None,
    **transmission_options,
):
    scan_id = 1
    num_baselines = 10
    num_pols = 4
    config = transmitters.create_config(num_streams=num_streams, **transmission_options)

    if not channels:
        channels = ChannelRange(0, 16)

    if channel_vis is not None:
        assert channel_vis.shape == (channels.count,)
        assert np.issubdtype(channel_vis.dtype, np.complexfloating)
        vis = np.broadcast_to(channel_vis, (num_baselines, num_pols) + channel_vis.shape)
        vis = np.swapaxes(vis, 1, 2)
    else:
        vis = np.zeros(
            shape=(num_baselines, channels.count, num_pols),
            dtype="<c8",
        )

    transmitter = await transmitters.create(
        config,
        TransmitterInitData(
            scan_id, num_baselines, channels, default_transmitter_low_init_data(channels, epoch)
        ),
    )
    async with transmitter:
        datum = CorrelatedDatum(
            unix_timestamp=time.time(),
            visibilities=vis,
            normalised_weights=np.ones([num_baselines, channels.count]),
        )
        await transmitter.send(datum)
    return transmitter.bytes_sent


@pytest.mark.asyncio
async def test_single_stream():
    await send_single_heap(1)


@pytest.mark.asyncio
async def test_multiple_streams():
    await send_single_heap(2)


def test_endpoint_parsing():
    endpoints = parse_endpoints("127.0.0.1:1")
    assert len(endpoints) == 1
    endpoints = parse_endpoints("127.0.0.1:1,127.0.0.1:2")
    assert len(endpoints) == 2
    endpoints = parse_endpoints("127.0.0.1:1-1000")
    assert len(endpoints) == 1000
    endpoints = parse_endpoints("127.0.0.1:1000-1000")
    assert len(endpoints) == 1
    assert parse_endpoints("127.0.0.1:1,127.0.0.1:2") == parse_endpoints("127.0.0.1:1-2")
    with pytest.raises(ValueError):
        parse_endpoints("127.0.0.1:1000-1")


def test_stream_endpoints():
    def _create_transmitter(num_streams, endpoints):
        config = transmitters.create_config(num_streams=num_streams, endpoints=endpoints)
        transmitter = _default_transmitter(config, 1024)
        asyncio.run(transmitter)

    # Enough endpoints, even more than needed
    _create_transmitter(1, "127.0.0.0:8000")
    _create_transmitter(2, "127.0.0.0:8000-8001")
    _create_transmitter(16, "127.0.0.0:8000-8015")
    _create_transmitter(16, "127.0.0.0:8000-9000")

    # Not enough endpoints for #streams
    with pytest.raises(ValueError):
        _create_transmitter(16, "127.0.0.0:8000-8014")


def test_tcp():
    """Simply checks that a TCP server receives the SPEAD data sent via TCP"""

    class CountingServer:
        async def communicate(self, reader, writer):
            self.bytes_received = 0
            while True:
                data = await reader.read(64 * 1024)
                if not data:
                    break
                if self.bytes_received == 0:
                    # magic version in all SPEAD packets
                    assert data[0:2] == b"\x53\x04"
                self.bytes_received += len(data)
            writer.close()

    async def send_to_counting_server():
        counting_server = CountingServer()
        tcp_server = await asyncio.start_server(counting_server.communicate, "127.0.0.1", 12346)
        async with tcp_server:
            bytes_sent = await send_single_heap(
                1, transport_protocol="tcp", target_port_start=12346
            )
        assert bytes_sent == counting_server.bytes_received

    asyncio.run(send_to_counting_server())


async def _exhaust_recv_stream(recv_stream: spead2.recv.asyncio.Stream):
    item_group = spead2.ItemGroup()
    sos_items = {}
    data_heap_items = {}
    async for heap in recv_stream:
        items = item_group.update(heap)
        if heap.is_start_of_stream():
            sos_items = items
        elif heap.is_end_of_stream():
            pass
        else:
            data_heap_items = items
    return item_group, sos_items.values(), data_heap_items.values()


@pytest.mark.asyncio
@pytest.mark.parametrize("cbf_icd", (LowICD, MidICD))
async def test_sos_heap_contents(cbf_icd):
    port = 23456
    recv_stream = create_tcp_recv_stream(port)

    now = time.time()
    bytes_sent, reception_results = await asyncio.gather(
        send_single_heap(
            1,
            epoch=now,
            target_port_start=port,
            telescope=cbf_icd.TELESCOPE,
            transport_protocol="tcp",
        ),
        _exhaust_recv_stream(recv_stream),
    )
    item_group, sos_items, data_heap_items = reception_results

    def item_ids(items):
        return frozenset(item.id for item in items)

    assert bytes_sent > 0
    assert item_ids(sos_items) == item_ids(cbf_icd.SENT_ON_SOS_HEAP)
    assert item_ids(data_heap_items) == item_ids(cbf_icd.SENT_ON_DATA_HEAP)

    is_low = cbf_icd == LowICD
    assert (ItemID.CBF_SOURCE_ID in item_group) == is_low
    assert (ItemID.CBF_SOURCE_ID in item_ids(sos_items)) == is_low
    if is_low:
        assert item_group[ItemID.CBF_SOURCE_ID].value == b"L"

        sps_epoch_item = next(i for i in sos_items if i.id == ItemID.SPS_EPOCH)
        expected_epoch = LowICD.sps_to_icd_epoch(LowICD.unix_to_sps(now))
        assert expected_epoch == sps_epoch_item.value


@pytest.mark.asyncio
@pytest.mark.parametrize("telescope", [t.value for t in Telescope])
@pytest.mark.parametrize(
    "channels, num_streams, expected_channels",
    [
        (ChannelRange(0, 16), 1, [ChannelRange(0, 16)]),
        (ChannelRange(0, 16), 2, [ChannelRange(8 * i, 8) for i in range(2)]),
        (ChannelRange(0, 16), 4, [ChannelRange(4 * i, 4) for i in range(4)]),
        (ChannelRange(100, 16), 1, [ChannelRange(100, 16)]),
        (
            ChannelRange(100, 16, 2),
            3,
            [
                ChannelRange(100, 6, 2),
                ChannelRange(112, 5, 2),
                ChannelRange(122, 5, 2),
            ],
        ),
    ],
)
async def test_sending_with_channel_range(
    telescope: Telescope,
    num_streams: int,
    channels: ChannelRange,
    expected_channels: list[ChannelRange],
):
    port = 24456

    now = time.time()
    ch_vis = np.arange(channels.count, dtype=np.complex64)
    results = await asyncio.gather(
        send_single_heap(
            num_streams,
            epoch=now,
            channels=channels,
            channel_vis=ch_vis,
            target_port_start=port,
            telescope=telescope,
            transport_protocol="tcp",
        ),
        *[_exhaust_recv_stream(create_tcp_recv_stream(port + i)) for i in range(num_streams)],
    )
    assert results[0] > 0
    item_groups = map(lambda r: r[0], results[1:])
    for expected_channel, item_group in zip(expected_channels, item_groups):
        assert expected_channel.start_id == item_group[ItemID.CHANNEL_ID].value

        vis: np.ndarray = item_group[ItemID.CORRELATOR_OUTPUT_DATA].value["VIS"]
        assert vis.shape[0] == expected_channel.count

        start_idx = (expected_channel.start_id - channels.start_id) // channels.stride
        expected_written_idxs = slice(start_idx, start_idx + expected_channel.count)
        assert np.all(vis == ch_vis[expected_written_idxs, np.newaxis, np.newaxis])

        if telescope == Telescope.MID:
            assert item_group[ItemID.CHANNEL_COUNT].value == expected_channel.count
