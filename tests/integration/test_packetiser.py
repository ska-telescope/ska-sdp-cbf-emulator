"""Tests for the high-level packetise function"""

import asyncio
import time
from typing import AsyncIterable

import numpy as np
import pytest
import spead2.recv.asyncio
from numpy.testing import assert_almost_equal
from overrides import override
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.icd import ItemID, LowICD, MidICD

from ska_sdp_cbf_emulator import packetiser, transmitters
from ska_sdp_cbf_emulator.data_source.correlated_data_source import (
    CorrelatedDataSource,
    CorrelatedDatum,
)
from ska_sdp_cbf_emulator.packetiser import MeasurementSetDataSourceConfig, SenderConfig
from tests.common import create_tcp_recv_stream


def _test_package(time_interval, scan_id=1, num_timestamps=0, num_repeats=1):
    config = SenderConfig(scan_id=scan_id, time_interval=time_interval)
    config.transmission = transmitters.create_config(num_streams=1)
    config.ms = MeasurementSetDataSourceConfig(location="data/sim-vis.ms")
    config.ms.num_timestamps = num_timestamps
    config.ms.num_repeats = num_repeats
    heaps_sent = asyncio.run(packetiser.packetise(config))

    # measurement set contains 4 channels and 133 timestamps
    if num_timestamps == 0:
        num_timestamps = 133
    assert heaps_sent == 2 + num_repeats * num_timestamps


def test_default_time_interval():
    _test_package(0, num_timestamps=2)


def test_fixed_time_interval():
    _test_package(0.001)


def test_no_time_interval():
    _test_package(-1)


def test_scans():
    _test_package(-1, scan_id=2, num_timestamps=3)


def test_independent_configuration_objects():
    config1 = SenderConfig()
    config1.sdp_config_db.backend = "blah"
    config1.transmission.method = "foo"
    config2 = SenderConfig()
    assert config1.sdp_config_db.backend != config2.sdp_config_db.backend
    assert config1.transmission.method != config2.transmission.method


@pytest.mark.parametrize(
    "scan_id, valid",
    ((-10, False), (-1, False), (0, False), (1, True), (2, True), (10, True)),
)
def test_scan_id_validation(scan_id, valid):
    if not valid:
        with pytest.raises(ValueError):
            SenderConfig(scan_id=scan_id)
    else:
        assert SenderConfig(scan_id=scan_id).scan_id == scan_id


class _MockDataSource(CorrelatedDataSource):
    def __init__(self, vis: complex, weight: float) -> None:
        self._epoch = time.time()
        self.vis_value = vis
        self.weight_value = weight

    @property
    @override
    def name(self):
        return "Mock"

    @property
    @override
    def info(self) -> list[tuple[str, str]]:
        return []

    @property
    @override
    def num_baselines(self) -> int:
        return 6

    @property
    @override
    def channels(self) -> ChannelRange:
        return ChannelRange(0, 10)

    @property
    @override
    def channel_frequencies(self) -> np.ndarray:
        """The frequencies of each channel"""
        return np.arange(self.channels.count) + 6

    @property
    @override
    def channel_resolution(self) -> float:
        """The resolution of each channel"""
        return 0.5

    @property
    @override
    def integration_period(self) -> float:
        return 0.9

    @property
    @override
    def visibility_epoch(self) -> float:
        return self._epoch

    @override
    async def data(self) -> AsyncIterable[CorrelatedDatum]:
        yield CorrelatedDatum(
            self._epoch,
            np.broadcast_to(self.vis_value, [self.num_baselines, self.channels.count, 4]),
            np.broadcast_to(self.weight_value, [self.num_baselines, self.channels.count]),
        )


async def _recv_single_heap(recv_stream: spead2.recv.asyncio.Stream):
    item_group = spead2.ItemGroup()
    received_data_heap = False
    async for heap in recv_stream:
        is_data_heap = not heap.is_start_of_stream() and not heap.is_end_of_stream()
        assert not is_data_heap or not received_data_heap

        if is_data_heap:
            received_data_heap = True

        item_group.update(heap)

    return item_group


@pytest.mark.asyncio
@pytest.mark.parametrize("cbf_icd", (LowICD, MidICD))
async def test_data_source_transmission(cbf_icd):
    """Verify the data of a single data heap matches what is sent by the emulator"""
    port = 23456
    recv_stream = create_tcp_recv_stream(port)

    config = SenderConfig(
        scan_id=1,
        subarray_id=2,
        beam_id=3,
        zoom_window_id=5,
        time_interval=-1,
        hardware_id=0xCAFE,
    )
    config.transmission = transmitters.create_config(
        num_streams=1,
        target_port_start=port,
        telescope=cbf_icd.TELESCOPE,
        transport_protocol="tcp",
    )
    data_source = _MockDataSource(0.3 + 0.2j, 0.95)

    bytes_sent, item_group = await asyncio.gather(
        packetiser.packetise_visibilities(config, data_source),
        _recv_single_heap(recv_stream),
    )

    def item_value(item_id):
        return item_group[item_id].value

    assert bytes_sent > 0
    assert data_source.channels.start_id == item_value(ItemID.CHANNEL_ID)
    assert config.hardware_id == item_value(ItemID.HARDWARE_ID)

    vis: np.ndarray = item_value(ItemID.CORRELATOR_OUTPUT_DATA)["VIS"]
    assert vis.shape[0] == data_source.channels.count
    assert vis.shape[1] == data_source.num_baselines
    assert np.all(data_source.vis_value == vis)

    corr_frac: np.ndarray = item_value(ItemID.CORRELATOR_OUTPUT_DATA)["FD"]
    assert corr_frac.shape[0] == data_source.channels.count
    assert corr_frac.shape[1] == data_source.num_baselines
    assert np.all(int(data_source.weight_value * 255) == corr_frac)

    if cbf_icd == LowICD:
        assert config.subarray_id == item_value(ItemID.SUBARRAY_ID)
        assert config.beam_id == item_value(ItemID.STATION_BEAM_ID)
        assert config.zoom_window_id == item_value(ItemID.ZOOM_WINDOW_ID)
        assert_almost_equal(data_source.integration_period, item_value(ItemID.INTEGRATION_PERIOD))
        assert_almost_equal(
            data_source.channel_resolution, item_value(ItemID.FREQUENCY_RESOLUTION)
        )
        assert_almost_equal(
            data_source.channel_frequencies[0], item_value(ItemID.CHANNEL_FREQUENCY)
        )

        sps_epoch = item_value(ItemID.SPS_EPOCH)
        expected_epoch = LowICD.sps_to_icd_epoch(LowICD.unix_to_sps(data_source.visibility_epoch))
        assert expected_epoch == sps_epoch
    elif cbf_icd == MidICD:
        assert data_source.channels.count == item_value(ItemID.CHANNEL_COUNT)
    else:
        raise ValueError("Unknown ICD!")
