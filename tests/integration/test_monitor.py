"""
Tests for SDP Configuration Database scan monitoring
"""

import asyncio
import logging
from contextlib import nullcontext
from typing import Optional, Type

import pytest
from ska_sdp_config import Config
from ska_sdp_config.config import Transaction
from ska_sdp_config.entity.eb import ExecutionBlock

from ska_sdp_cbf_emulator import transmitters
from ska_sdp_cbf_emulator.emulator import SdpConfigScanMonitor, arun_cbf_emulator
from ska_sdp_cbf_emulator.packetiser import MeasurementSetDataSourceConfig, SenderConfig

logger = logging.getLogger(__name__)

EB_ID = "eb-test-00000000-0000"
PB_ID = "pb-test-00000000-0000"


@pytest.fixture(name="sdp_config")
def sdp_config_fixture(backend: str):
    """A Config client that is emptied before and after it's used."""
    sdp_config = Config(backend=backend)
    sdp_config.backend.delete("/pb", recursive=True, must_exist=False)
    sdp_config.backend.delete("/eb", recursive=True, must_exist=False)
    yield sdp_config
    sdp_config.backend.delete("/pb", recursive=True, must_exist=False)
    sdp_config.backend.delete("/eb", recursive=True, must_exist=False)


@pytest.fixture(name="monitor")
def _monitor_fixture(sdp_config: Config):
    monitor = SdpConfigScanMonitor(EB_ID, sdp_config)
    yield monitor


@pytest.mark.asyncio
@pytest.mark.parametrize("backend", ["memory", "etcd3"])
async def test_config_scan_monitor_without_eb(monitor: SdpConfigScanMonitor):
    """Test scan monitor behaviour when execution block does not exist."""
    with pytest.raises(RuntimeError):
        await monitor.arun()


@pytest.mark.asyncio
@pytest.mark.parametrize("backend", ["memory", "etcd3"])
async def test_config_scan_monitor_stop(sdp_config: Config, monitor: SdpConfigScanMonitor):
    """
    Test that the monitor thread and coroutine will end if stop is called.
    """
    # create eb
    for txn in sdp_config.txn():
        txn.execution_block.create(ExecutionBlock(key=EB_ID))
        txn.execution_block.state(EB_ID).create(
            {
                "scan_type": None,
                "scan_id": None,
                "scans": [],
                "pb_receive_addresses": None,
            }
        )

    monitor_task = asyncio.create_task(monitor.arun())

    async def close():
        await asyncio.sleep(1)
        monitor_task.cancel()

    close_task = asyncio.create_task(close())

    (done, pending) = await asyncio.wait([monitor_task, close_task], timeout=3)
    assert len(pending) == 0
    assert len(done) == 2
    for task in done:
        await task


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "error_kind,error_type",
    [
        (None, None),
        ("packetise", TypeError),
        ("send", TypeError),
        ("pb", KeyError),
        ("eb", ValueError),
    ],
)
@pytest.mark.parametrize("backend", ["memory", "etcd3"])
async def test_config_scan_monitor(
    sdp_config: Config,
    monitor: SdpConfigScanMonitor,
    error_kind: Optional[str],
    error_type: Optional[Type],
):
    """Test that the monitor can run detecting issues if they are present."""
    sender_config = SenderConfig()
    sender_config.ms = MeasurementSetDataSourceConfig(location="data/sim-vis.ms")
    sender_config.transmission = transmitters.create_config(num_streams=1)
    sender_config.time_interval = -1 if error_kind != "packetise" else "invalid"
    if error_kind == "send":
        sender_config.transmission.delay_start_of_stream_heaps = "invalid"

    # create eb
    for txn in sdp_config.txn():
        txn.execution_block.create(ExecutionBlock(key=EB_ID))
        txn.execution_block.state(EB_ID).create(
            {
                "scan_type": None,
                "scan_id": None,
                "scans": [],
                "pb_receive_addresses": None,
            }
        )

    async def scan_and_stop():
        for txn in sdp_config.txn():
            configure(txn, error_kind)
            scan(txn, 1, error_kind)
        await asyncio.sleep(1)  # scan for 1 second

        for txn in sdp_config.txn():
            scan(txn, 2)
        await asyncio.sleep(1)

        # NOTE: must_exist=False as only child path is used
        for txn in sdp_config.txn():
            txn.execution_block.delete(EB_ID, recurse=True)
            txn.processing_block.delete(PB_ID, recurse=True)
            logger.debug("deleted eb/ and pb/")

    with pytest.raises(error_type) if error_type else nullcontext():
        await asyncio.gather(
            scan_and_stop(),
            arun_cbf_emulator(monitor, sender_config),
        )
        # TODO(YAN-1139): assert packets are transmitted
        # and can be received for each scan.


def configure(txn: Transaction, error_kind: str | None = None):
    """Configure receive PB state with host port mapping."""
    logging.info("configuring target")
    txn.processing_block.state(PB_ID).create(
        {
            "receive_addresses": (
                {
                    "target:a": {
                        "vis0": {
                            "host": [[0, "127.0.0.1"]],
                            "port": [[0, 4000]],
                        }
                    }
                }
                if error_kind != "pb"
                else {}
            )
        },
    )


def scan(txn: Transaction, scan_id: int, error_kind: str | None = None):
    """Command config database to start a scan."""
    eb_state = {
        "eb_id": EB_ID,
        "scan_id": scan_id,
        "scan_type": "target:a",
        "pb_receive_addresses": PB_ID if error_kind != "eb" else None,
    }
    txn.execution_block.state(EB_ID).update(eb_state)
