"""Shared convenience functions"""

import time

import numpy as np
import spead2.recv.asyncio
from realtime.receive.core.channel_range import ChannelRange

from ska_sdp_cbf_emulator.transmitters import TransmitterInitData, TransmitterLowInitData


def create_tcp_recv_stream(port: int):
    """Create a TCP SPEAD2 receive stream"""
    io_thread_pool = spead2.ThreadPool()
    config = spead2.recv.StreamConfig(max_heaps=1, stop_on_stop_item=False)
    ring_config = spead2.recv.RingStreamConfig(heaps=1, contiguous_only=False)
    stream = spead2.recv.asyncio.Stream(io_thread_pool, config, ring_config)
    stream.add_tcp_reader(port)
    return stream


def default_transmitter_init_data(channels: int | ChannelRange) -> TransmitterInitData:
    """Create a `TransmitterInitData` instance with default, hardcoded values"""
    if isinstance(channels, int):
        channels = ChannelRange(0, channels)

    return TransmitterInitData(
        scan_id=0,
        num_baselines=0,
        channels=channels,
        low=default_transmitter_low_init_data(channels.count),
    )


def default_transmitter_low_init_data(
    num_channels: int | ChannelRange,
    visibility_epoch=time.time(),
) -> TransmitterLowInitData:
    """Create a `TransmitterLowInitData` instance with default, hardcoded values"""
    if isinstance(num_channels, ChannelRange):
        num_channels = num_channels.count

    return TransmitterLowInitData(
        subarray_id=1,
        beam_id=1,
        zoom_window_id=1,
        integration_period_s=0.9,
        channel_frequencies_hz=np.arange(num_channels),
        channel_resolution_hz=1.0,
        visibility_epoch=visibility_epoch,
    )
