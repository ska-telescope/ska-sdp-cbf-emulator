#!/bin/sh

cleanup=0
if [ $# -ge 1 ] && [ "$1" = "-c" ]; then
	cleanup=1
fi

if [ -d sim-vis.ms ]; then
	echo Removing product
	rm -rf sim-vis.ms
fi

IMAGE_NAME=sim_image:temp
FILES_TO_EXTRACT="config.ini run.sh sim-vis.ms csim_N006_ch00000.in"

echo Building simulation using parameters in generate_inputs.py and tools in Yandsoft image
docker build -t ${IMAGE_NAME} -f build-sim.docker .
echo Creating image so we can get the artifact out
docker container create --name extract ${IMAGE_NAME}

echo Extracting files
for fname in $FILES_TO_EXTRACT; do
	docker container cp extract:/home/simulation/$fname ./$fname
done

docker rm -f extract
if [ -d sim-vis.ms ]; then
	echo Success
	ret=0
else
	echo No product built
	ret=1
fi

if [ $ret = 0 ] && [ $cleanup = 1 ]; then
	echo Cleaning up extracted files after a successful run
	for fname in $FILES_TO_EXTRACT; do
		rm -rf ./$fname
	done
fi

exit $ret
