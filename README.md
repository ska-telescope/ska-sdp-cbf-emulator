CBF-SDP Interface Emulator
===========================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-cbf-sdp-emulator/badge/?version=latest)](https://developer.skatelescope.org/projects/cbf-sdp-emulator/en/latest/?badge=latest)


This is the python package to emulate the CBF-SDP interface developed as part of Feature SP-794

## Installation

Installation instructions can be found in the online [documentation](https://developer.skatelescope.org/projects/cbf-sdp-emulator/en/latest/?badge=latest).
