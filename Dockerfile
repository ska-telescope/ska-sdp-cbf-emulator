# =============================================================================
# Visibility Send/Receive image : build stage
# =============================================================================

ARG PYTHON_VERSION=3.10

FROM python:${PYTHON_VERSION} AS buildenv
ARG POETRY_VERSION=1.8.2
RUN pip install poetry==${POETRY_VERSION}

WORKDIR /app
COPY ./ ./

RUN poetry config virtualenvs.in-project true \
    && poetry install --only main --no-root \
    && . .venv/bin/activate \
    && pip install --no-deps .

FROM python:${PYTHON_VERSION}-slim AS runtime
ARG IMAGE_VERSION=7.0.0
LABEL \
    author="Rodrigo Tobar, Stephen Ord, Callan Gray" \
    description="An image for the reception and transmission of visibilities" \
    license="BSD-3-Clause" \
    vendor="SKA Telescope" \
    org.skatelescope.team="YANDA" \
    org.skatelescope.website="https://gitlab.com/ska-telescope/ska-cbf-sdp-emulator/"

COPY --from=buildenv /app/.venv /app/.venv/
ENV PATH="/app/.venv/bin:${PATH}"
