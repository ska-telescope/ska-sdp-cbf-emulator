Configuration options
=====================

When using the programmatic interface of this package,
the configuration options used by the different parts of the sending operation
are given as objects.
See |sender-config|, |ms-config|, |hardcoded-config|, |sdp-config-db-config|,
|transmission-config| and |spead2-config|
for details.


From nested dictionaries
------------------------

When using the command-line interface,
options can be specified either in a YAML configuration file,
or individually via the :option:`emu-send -o` option.
In both cases options live within a group or category,
and have a name and a value,
and thus the overall configuration can be specified
as a top-level dictionary
with group names as keys,
and group dictionaries as values.
To preserve backwards compatibility,
the programmatic entry points
still accept this dictionary-based configuration approach too.

These options are used to populate
a |sender-config| object
as follows:

* Within each group,
  the key names are directly mapped
  to the corresponding configuration object member.
* The ``sender`` group is used to populate
  the options in the top-level |sender-config| object.
* The ``ms`` (previously ``reader``) group is used to populate
  the options in the |ms-config| object
  under :attr:`ska_sdp_cbf_emulator.packetiser.SenderConfig.ms`.
* The ``hardcoded`` group is used to populate
  the options in the |hardcoded-config| object
  under :attr:`ska_sdp_cbf_emulator.packetiser.SenderConfig.ms`.
* The ``sdp_config_db`` group is used to populate
  the options in the |sdp-config-db-config| object
  under :attr:`ska_sdp_cbf_emulator.packetiser.SenderConfig.sdp_config_db`.
* The ``transmission`` group is used to populate
  the options in the |transmission-config| (or subclass) object
  under :attr:`ska_sdp_cbf_emulator.packetiser.SenderConfig.transmission`.

.. |sender-config| replace:: :class:`SenderConfig <ska_sdp_cbf_emulator.packetiser.SenderConfig>`
.. |ms-config| replace:: :class:`MeasurementSetDataSourceConfig <ska_sdp_cbf_emulator.data_source.measurement_set.MeasurementSetDataSourceConfig>`
.. |hardcoded-config| replace:: :class:`HardcodedDataSourceConfig <ska_sdp_cbf_emulator.data_source.hardcoded.HardcodedDataSourceConfig>`
.. |sdp-config-db-config| replace:: :class:`SdpConfigDbConfig <ska_sdp_cbf_emulator.packetiser.SdpConfigDbConfig>`
.. |transmission-config| replace:: :class:`Config <ska_sdp_cbf_emulator.transmitters.Config>`
.. |spead2-config| replace:: :class:`Spead2TransmissionConfig <ska_sdp_cbf_emulator.transmitters.spead2_transmitters.Spead2TransmissionConfig>`
