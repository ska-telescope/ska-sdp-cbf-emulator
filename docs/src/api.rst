.. doctest-skip-all
.. _package-guide:


API documentation
=================

This section describes the main entry points for the emulator API.
While most users will be using the :program:`emu-send` program,
the sender code can be embedded directly into arbitrary python programs,
like in the case of the :doc:`cbf-sdp-emulator-tango-device:index`.

``ska_sdp_cbf_emulator.packetiser`` module
------------------------------------------

.. automodule:: ska_sdp_cbf_emulator.packetiser
    :members:

``ska_sdp_cbf_emulator.emulator`` module
----------------------------------------

.. automodule:: ska_sdp_cbf_emulator.emulator
    :members:

``ska_sdp_cbf_emulator.transmitters`` module
--------------------------------------------

.. autoclass:: ska_sdp_cbf_emulator.transmitters.Config
    :members:

``ska_sdp_cbf_emulator.transmitters.spead2_transmitters`` module
----------------------------------------------------------------

.. automodule:: ska_sdp_cbf_emulator.transmitters.spead2_transmitters
    :members:

