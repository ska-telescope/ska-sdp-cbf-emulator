Installation
============

CAR
---

This program is distributed by python wheels
and all dependencies should be automatically pulled.
The only caveat is that some of the dependencies
are found in the CAR server instead of PyPI,
so we need to point ``pip`` to the CAR in order to them::

  # Install directly via pip from the CAR repository
  pip install --extra-index-url=https://artefact.skao.int/pypi-all/simple ska-sdp-cbf-emulator


Poetry (Developer)
------------------

This project requires packaging using Poetry before being installation.
Poetry can be installed to OS python environment using either of the
following::

  # System user install
  curl -sSL https://install.python-poetry.org | python3 -

  # Install latest version on PyPI
  pip install -U poetry

For local development and packaging, Poetry can be used in several ways::

  # Go into the top-level directory of this repository
  cd ska-cbf-sdp-emulator

  # (if using pyenv) Use pyenv for testing against a specific python version. It is
  # recommended to regularly test versions used by continuous integration.
  pyenv local 3.10.6

  # (if using poetry env) Use Poetry virtual environment to install and use
  # project dependencies and package in isolation from python global packages.
  poetry env use 3.10.6

  # Regenerate the lock file for the active python environment (poetry commands
  will always use a virtual environment if one is found).
  poetry lock

  # Development install to the currently active python environment. This will
  # setup a .pth in site-packages that points to the development directory.
  poetry install

  # Test commands within the poetry virtual environment, e.g.
  poetry run pytest

  # Alternatively can use the poetry shell instead of `poetry run`.
  poetry shell
  pytest

  # (if using poetry shell) Deactivate Poetry shell
  exit

  # (if using poetry env, optional) To remove/delete a Poetry environment run 
  # the following from the top-level directory 
  poetry env remove 3.10.6

  # (if using pyenv, optional) Remove association with pyenv python version
  pyenv local --unset

Once changes are made and tested, a python wheel may be generated using Poetry
to the dist/ directory that can be installed via pip. Building wheel inside a
poetry virtual environment will mark the wheel with the specific python version::

  # Build a local development wheel
  poetry build
  pip install dist/*.whl

This is performed automatically when using pip::

  # Use pip to install the project (note: -e editable mode does not work here)
  pip install --extra-index-url=$PYPI_REPOSITORY_URL/simple .
