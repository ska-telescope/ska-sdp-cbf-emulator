Running
=======

Programmatically
----------------

There are two main programmatic entry points to run this package:

* The :func:`ska_sdp_cbf_emulator.packetiser.packetise` coroutine
  takes an input Measurement Set,
  reads data from it as per the given configuration,
  and transmits it over the network.
* The :func:`ska_sdp_cbf_emulator.emulator.arun_cbf_emulator` coroutine
  continuously monitors the SDP Configuration Database
  to detect when a Scan for a given Execution Block ID
  has started or stopped.
  When a Scan starts,
  it invokes :func:`ska_sdp_cbf_emulator.packetiser.packetise`
  to send data tagged for that Scan.
  When a Scan stops,
  the current sending operation is cancelled,
  if still running.

Command-line
------------

An :program:`emu-send` program should be available after installing the package.
This program takes a Measurement Set and transmits it over the network
using the preferred transmission method.

.. program:: emu-send

.. option:: measurement_set

   The measurement set to read data from

.. option:: -eb execution_block_id

   An execution block id to monitor for scans

.. option:: -c config

   A YAML configration file to read options from

.. option:: -o option

   Additional configuration options in the form of ``group.name=value``

.. option:: -q quiet

   Additional parameter to silence info logging from standard output

Tango device wrapper
--------------------

A Tango device wrapping the emulator sender
is available under :doc:`cbf-sdp-emulator-tango-device:index`.
The purpose of this Tango device
is to be used as a simulation of the real CBF,
making it possible to run a full end-to-end SKA system
that exercises the visibility data flow.

In SDP
------

In the context of the :doc:`ska-sdp-integration:index`
the emulator is deployed as a Helm chart
to exercise the visibility receive workflow.
