.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.


CBF-SDP Emulator
================

This is an emulator for the Correlator Beamformer
and its data sending capabilities.
It reads data from a data source
(typically a Measurement Set,
but other sources will be added in the future,
like correlator data dumps)
and sends it over a network transport
to a number of receivers,
thus mimicking the CBF-SDP interface.

The Low and Mid SKA telescopes have different ICDs
specifying the data that should be sent from the CBF to SDP.
This emulator implements both modes to the best extent possible,
although differences with the real hardware might exist.

This is an extensible and configurable package
that has been designed to support multiple communication protocols
and provide a platform for testing consumers of CBF data payloads.
It currently only implements the SPEAD transmission protocol,
but other protocols can be added in the future.

.. toctree::
  :maxdepth: 2

  installation
  running
  configuration
  api
