Change Log
###########

Development version
-------------------

9.0.0
-----

Added
-----

 * Support for SDP v1 schemas.
 * **BREAKING** Bump dependency on ``ska-sdp-config`` to ``^1.0.0``.

8.3.2
-----

Changed
^^^^^^^

Further raised the dependency on ska-sdp-config to 0.10.0.


8.3.1
-----

Changed
^^^^^^^

Allowed the dependency on realtime-receive-core 
to be 6.5.0 or higher.

8.3.0
-----

Changed
^^^^^^^

* Updated dependency on ska-sdp-config to 0.9.0.

8.2.0
-----

Changed
^^^^^^^

* Some non-fuctional changes to maintain compatibility with > 0.7.0 of the sdp-config
* Bumped the dependency on realtime-receive-core to 6.5.0 to pick up the fixes to the start-time

8.1.0
-----

Added
^^^^^

- The CBF emulator now supports sending correlated data fractions
  from the specified ``CorrelatedDataSource`` over SPEAD.
  Notably, the ``MeasurementSetDataSource`` will now populate these values
  from the specified Measurement Set.
- Previously hardcoded start-of-stream item ``HARDWARE_ID`` is now able to be
  configured.
- When emulating LOW, previously hardcoded start-of-stream items ``STATION_BEAM_ID``
  ``SUBARRAY_ID``, ``FREQUENCY_RESOLUTION``, ``CHANNEL_FREQUENCY``,
  ``ZOOM_WINDOW_ID`` and ``INTEGRATION_PERIOD`` are able to be configured
  or are populated from the source MS.

Changed
^^^^^^^

- Require poetry ``>= 1.8.2``.
- Require astropy ``^6.1``. This is because an important upstream dependency 
  (ska-sdp-func-python) has been updated to require astropy 6.1.

8.0.0
-----

Changed
^^^^^^^

* ``MeasurementSetDataSourceConfig`` and ``HardcodedDataSourceConfig``
  channel related properties now *only* refer to the channel ids that will be
  sent alongside the sent visibilities instead of (in the case of
  ``MeasurementSetDataSourceConfig``) also controlling what data is read
  from the data source.
* Updated dependency on ska-realtime-receive-core to ``^6.1.0``.

Removed
^^^^^^^

* **BREAKING** ``MeasurementSetDataSourceConfig.num_channels`` has
  been removed. All channels from the measurement set will now always
  be transmitted.


7.0.0
-----

Added
^^^^^

* New hardcoded emulation mode that can be specified using
  the ``hardcoded`` configuration path. For more details
  see the documentation.

Changed
^^^^^^^

* **BREAKING** Changed top-level package name
  from ``cbf_sdp`` to ``ska_sdp_cbf_emulator``
  to better align with SKA coding standards.

Deprecated
^^^^^^^^^^

* The ``reader`` config path has been renamed to ``ms``
* The input measurement set should now be specified
  using the ``ms.location`` configuration option rather than
  directly on the command line

Fixed
^^^^^

* Fix ``reader.start_channel`` being ignored when reading from the input
  measurement set. Previously visibilities would always start being read
  from the first channel in the measurement set.

Removed
^^^^^^^

* **BREAKING** Remove deprecated configuration options

6.1.0
-----

Changed
^^^^^^^

* Update dependency on spead2 to ``^4.0.2``.

6.0.0
-----

Changed
-------

* **BREAKING** Removed support for dict-based configuration
  in high-level API endpoints.
* **BREAKING** The file pointed to
  by the ``-c`` / ``--config`` command line option
  of ``cbf-emu``
  is now a YAML file instead of an INI file.
* Updated dependency on ``ska-sdp-realtime-receive-core`` to ``^6.0.0``.

5.0.0
-----

Changed
^^^^^^^

* Sender raises an error when configured
  to send data for an invalid scan ID
  (valid scan IDs are positive numbers).
* Updated dependency on ``ska-sdp-config`` to ``^0.5.1``
  to get newer, more performant backend.


5.0.0-alpha.1
-------------

Added
^^^^^

* Add ``reader.channel_stride`` option which
  allows sending of non-contiguous channels to the receiver.

Changed
^^^^^^^

* The ``transmission.buffer_size`` default value
  now depends on the chosen protocol
  (it previously was hard-coded to be spead2's default value
  for UDP streams).
  It is also automatically adjusted
  to not go over the OS settings for maximum buffer sizes,
  thus avoiding warnings when the streams are created.
* **BREAKING** ``reader.start_channel`` now also controls the
  ``CHANNEL_ID`` that gets sent over SPEAD (previously this just
  controlled the visibilities that were read from the measurement
  set, and ``CHANNEL_ID`` always started from ``0``).

4.4.1
-----

Fixed
^^^^^

* Send ``SPS_EPOCH`` with start-of-stream heap when emulating
  LOW


4.4.0
-----

New Features
^^^^^^^^^^^^

* First implementation of the new Low ICD,
  which is now different from the Mid ICD.
  Sending with Low ICD defaults to 1 channel per stream (as per the ICD),
  but this can be overridden for testing purposes.
* A new ``transmission.telescope`` option
  allows users to select the telescope
  that should be emulated.

Changes
^^^^^^^

* Added CCI data when sending data for SKA Mid.
* Updated dependency on receive-core to ``^4.0``
  and on Python to ``^3.10``.

4.3.0
-----

New Features
^^^^^^^^^^^^

* A new ``transmission.num_streams`` option governs
  the number of streams a sender should send data through.
  With this new option we also correctly handle the case
  when the number of channels is not a multiple of the number of streams,
  in which case channels get distributed as evenly as possible.

Deprecations
^^^^^^^^^^^^

* The ``transmission.channels_per_stream`` option is now deprecated
  in favour of the ``transmission.num_streams`` option.

4.2.0
-----

New Features
^^^^^^^^^^^^

* Added configuration classes
  for easier internal handling of configuration options.

Improvements
^^^^^^^^^^^^

* Allow ``cbf_sdp.emulator.arun_cbf_emulator``
  to take an Execution Block ID as parameter
  for easier usage as a high-level API entry point.
* Make clearer in the documentation
  that both ``cbf_sdp.packetiser.packetise``
  and ``cbf_sdp.emulator.arun_cbf_emulator``
  are the two high-level API entry points
  for this package.

Deprecations
^^^^^^^^^^^^

* Passing a dict-like object to ``cbf_sdp.packetiser.packetise``.
  Users should use the new ``cbf_sdp.packetiser.SenderConfig`` class instead.
* The following configuration items have been renamed
  (but are still available for backwards-compatibility):

  * ``transmission.scan_id`` -> ``sender.scan_id``
  * ``transmission.time_interval`` -> ``sender.time_interval``
  * ``reader.start_chan`` -> ``reader.start_channel``
  * ``reader.num_chan`` -> ``reader.num_channels``


4.1.0
-----

* Changed project build from setuputils to poetry
* Update build system to use ska-cicd-makefile
* Added new ``transmission.delay_start_of_stream_heaps`` option
  used to test the receiver's ability
  to handle out-of-order and missing start-of-stream heaps.

4.0.1
-----

* Update dependency on receive-core to >= 3.3
  to allow using this package together with other packages
  that depend on newer versions of receive-core.

4.0.0
-----

* Changed to ``-eb`` option for SDP database scan monitoring mode.
* Added support for URL measurement sets.

3.3.0
-----

* Changed config option ``reader.scan_ids`` to ``transmission.scan_id``
* Added ``execution_block_id`` argument for SDP Configuration
  Database monitoring mode.
* Correlated data fraction information,
  hardcoded to be all ones,
  is now being sent on each heap.

3.2.0
-----

* Add new ``transport_protocol`` option
  to indicate the network transport used by spead2.
  Supported values are ``tcp`` and ``udp``, defaults to ``udp``.

3.1.0
-----

* Use central ICD Item definitions from receive-core.
* Remove references to old plasma dependency.
* Send Item Descriptors only on the start-of-stream heap
  rather than on all heaps.

3.0.0
-----

* Added support for configuring scan_ids

2.0.4
-----

* Added ``transmission.endpoints`` option
  to send data to arbitrary endpoints
  instead of being bound to a single target host.
* Added ``transmission.buffer_size`` option
  to set the sender socket buffer size.
* Sender now reports heaps/s sending rate alongside MB/s.
* Removed explicit loop passing on transmitter creation routines.
* Added command line option to start sender in quiet mode.

2.0.2
-----

Removed some of the old BDD as they created a circular dependency. Updated some interfaces to the realtime.receive.core

2.0.1
-----

Removed all the receive and utilty code from here and placed it into 2 other repos in the realtime.receive namespace


1.6.11
------

Reversed the logic in the handling of external models - so now scheduling block based models are prioritised


1.6.10
------

Adding the functionality to create a measurement set from the scheduling block. This replaces the need to have a data model. Essentially I have refactored the TM classes to inherit from a BaseTM abstract base class - there are now three:

The original FakeTM which takes a measurement set in construction
The PlasmaTM which uses the contents of the plasma store in construction
[new] SchedTM which uses a scheduling block instance.

The 'get_nearest_data" is overloaded in these cases to simply return an empty UVW vector. Which is added to the measurement set. This seems to work fine in all the test cases - but I have had to remove checking the 'value' of the UVW tables in the MSAsserter used in the unit tests - as there are no UVW.

* Added schedblock parameter that can be used instead of datamodel for telescope model constrution
* Removed the requirement to test the value of the UVW table in the tests

1.6.9
-----

* Added mswriter parameters 'timestamp_output', 'command_template' and 'max_payloads'
* Added receive loop with 'continuous_mode' parameter
* Added timestamped ms names

1.6.8
-----

* Bugfix for receiving data outside model timerange
* Hide ms opening log messages

1.6.7
-----

* Added ms-asserter entrypoint for testing pipeline outputs
* Added configurable ring buffer option to mswriter
* emu-send repeat option increments now timestamps
* emu-recv updated to allow receiving timestamps beyond the model range

1.6.6
-----

* Removed the copy from the Dockerfile. This will increase the size - but should fix
  the issues related to missing functionality in the image.

1.6.5
-----

* Small update to the default Docker image to include plasma

1.6.4
-----

* Added Dockerfiles for dockerised tests of emulator and receive workflows

1.6.3
-----

* changed the repo name again to place it in the sdp sub-group.
* Updated the dependencies on the DAL.

1.6.2
-----
* changed the repo name
* added the .release file


1.6.1
-----

* Version not released due to issues with publication on the CAR

1.6
---

* Added optional support for using plasmaStMan for the DATA column
  of the main table.
* Added option to execute commands
  for each newly created Measurement Set
  with ``plasma-mswriter``.
* Corrected spectral window values when writing measurement sets.
* Added a null_consumer that provides an example for developers. Functionally just drops the payloads
* Added new BDD tests and data for AA0.5LOW configurations
* Changed the functionality of ``reader.num_repeats`` - it now resends the number of timestamps set by
  ``reader.num_timestamps`` the stated number of times - each separated by ``transmission.time_interval``.

1.5
---

* Added new ``transmission.time_interval`` option
  to control the time to let pass between transmissions of data
  of successive timestamps.
  By default this is calculated
  from the ``TIME`` values of the input Measurement Set,
  but arbitrary, fixed intervals, as well as null intervals,
  can be specified.
* Removed our dependency on OSKAR to read and write Measurement Sets.
  The code now uses the python casacore bindings instead,
  leading to a simpler installation procedure,
  both for developers and users.
* Ported code to use new name/version of the SKA logging library.
* Fixed and simplified configuration requirements
  of few of the tools and modules,
  so running the system requires less values
  to be explicitly given in order to run.
* Added automatic publishing of python package to Nexus
  when tags are pushed.

1.4
---

* Ported the plasma caller and processor
  to use Arrow Tables to transport visibility metadata
  rather than binary tensors with pickled content.
  This makes it possible to invoke processors
  that do not use our data types,
  and that are potentially written in a different language.
* The schemas defining the Tables and procedures
  are imported from the ``ska-sdp-dal-schemas`` package.

1.3
---

* Fixed a problem with a test failing
  when the plasma extra was not installed.
* Pin sdp-dal-prototype dependency
  to avoid breaking changes upstream.
* Removed dependency on ``astropy``.
  This package adds ~50 [MB] of data to our installations,
  while in return we use a single bit of functionality
  that is easily implemented.
* Remove old, unnecessary dependency on ``sh`` package.

1.2
---

* Added ability to specify user-provided consumer classes,
  which will allow us to use third-party consumer code
  without us having to maintain it.
* Added ``unix_time`` property to ``Payload`` class,
  which is compatible with values from the ``time`` built-in module.
* Updated to to work with spead2 versions 2 and 3.
* Updated test infrastructure to work
  against latest version of pytest-bdd.

1.1
---

* Developed new reception consumer
  that writes incoming payload data
  into an Apache Plasma store
  using the `sdp-dal-prototype <https://gitlab.com/ska-telescope/sdp-dal-prototype>` code.
  In the sdp-dal-prototype parlance this is a "Caller".
* Developed a corresponding "Processor"
  that reads the data off the Plasma store
  and writes it into a Measurement Set.
* General bug fixes and improvements.


1.0.1
-----

* Minor adjustments
  to example data used by demonstrations.

1.0
---

* First public release.
* SPEAD-based transmission and reception of ICD payloads
  implemented.
* Receivers use a generic consumer architecture
  to handle incoming payloads offered.
* Specific consumer implemented
  that takes incoming payloads
  and write them into a Measurement Set.
* It is possible to transmit data for multiple channels
  through a single SPEAD stream.
* UDP multicast transmission works.
* Simple Helm charts demonstrating functionality
  are available as examples.

0.3
---

* Further fixes, still not fully operational

0.2
---

* First tag, still not fully operational
